from base import celery
from datetime import datetime
import time
import parameters
import urllib.parse
from pymongo import MongoClient
from bs4 import BeautifulSoup
import requests
import auxiliary
from flask import current_app
import json
import os
#%% setup database connection
# def db_connection(collection):
#     username = urllib.parse.quote_plus('root')
#     password = urllib.parse.quote_plus('metadata@nmo2006')
#     mongo_client = MongoClient("mongodb://%s:%s@%s:27017" % (username, password, parameters.LocalRunningServer))
#     db = mongo_client[collection] # database
#     return db
# setup database connection
def db_connection(collection):
    # username = urllib.parse.quote_plus('root')
    # password = urllib.parse.quote_plus('metadata@nmo2006')
    username = urllib.parse.quote_plus(os.environ.get('DB_USER', 'root'))
    password = urllib.parse.quote_plus(os.environ.get('DB_PASS', 'metadata@nmo2006'))
    db_host = os.environ.get('DB_HOST', 'cng-nmo-dev6.orc.gmu.edu')
    mongo_client = MongoClient('mongodb://%s:%s@%s:27017' % (username, password, db_host))
    db = mongo_client[collection] # database
    return db

#%% test func
@celery.task
def make_file(fname, content):
    with open(fname, "w") as f:
        f.write(content)

#%% request call
@celery.task
def callback_api(pmid, save_only=False, debug_mode=True):
    # api url for return
    if debug_mode:
        print('running in test mode')
        url = "http://127.0.0.1:8000/api/nermetadata/update_metadata/" # local debug
    else:
        url = "{}nermetadata/update_metadata/".format(parameters.ExternalMetadataPortal) # in production
    # read the extracted information from the text and put it into the form
    db2 = db_connection('structured_data')
    query = db2.articles.find_one({'pmid':pmid}, {'_id': False})
    if query and 'metadata' in query:
        metadata = query['metadata']
    else:
        return "metadata does not exist for the requested pmid - call back api abrupted!"
    data = {
        'pmid': pmid,
        'ner_data': metadata,
        'save_only': save_only
    }
    msg = requests.post(url, json = data)
    return msg.text

# flatten json format and extract paragraphs
def flatten_json(d):
    skip = ['authorGroup', 'image', 'referenceList', 'id', 'affiliationList', 'contactId']
    out = {}
    content = []
    def flatten(x, name=''):
        if type(x) is dict:
            for k, v in x.items():
                if k in skip: continue
                flatten(v)
        elif type(x) is list:
            for a in x:
                flatten(a)
        else:
            if x is not None and len(x):
                content.append(re.sub(r"[\n\t\s]+", " ", str(x)).strip())
    flatten(d)
    return content
# flatten json format while keeping the strucutre of the article
def flatten_json2(d):
    skip = ['authorGroup', 'image', 'referenceList', 'id', 'affiliationList', 'contactId', 'contentType', 'category', 'label']
    content = {}
    def flatten(x, name=''):
        if type(x) is dict:
            for k, v in x.items():
                if k in skip: continue
                nme = name + ':' + k if len(name) else k
                flatten(v, nme)
        elif type(x) is list:
            for a in x:
                if 'title' in a and 'sectionList' in a:
                    flatten(a['sectionList'], name + ':' + a['title'])
                elif 'title' in a and 'paragraphList' in a:
                    flatten(a['paragraphList'], name + ':' + a['title'])
                else:
                    flatten(a, name)
        else:
            if x is not None and len(x):
                if name in content:
                    content[name].append(re.sub(r"[\n\t\s]+", " ", str(x)).strip())
                else:
                    content[name] = [re.sub(r"[\n\t\s]+", " ", str(x)).strip()]
    flatten(d)
    return content

#%% prepare for machine learning
@celery.task
def ml_prepare(pmid, debug_mode=True):
    # read the extracted information from the text and put it into the form
    db2 = db_connection('structured_data')
    query = db2.articles.find_one({'pmid':pmid}, {'_id': False})
    if query and 'fulltext' in query:
        article_data = query['fulltext']
    else:
        return "fulltext does not exist for the requested extraction - Entity Extraction abrupted"
    # flatten text from the json format
    article_text = flatten_json2(article_data)
    # extract name entities from the text
    if len(article_text) == 0:
        return "no full text for extraction"
    metadata_extract(pmid, article_text) # uncomment when testing is done
    # make excel file
    # excel_maker(pmid) # deactivated since it was not used for a while and json file structure changed. needs modificaion if uses
    # prepare a return api call
    db2 = db_connection('structured_data')
    query = db2.articles.find_one({'pmid':pmid}, {'_id': False}) # renew the query
    if query and 'metadata' in query:
        metadata = query['metadata']
    else:
        return "metadata does not exist for the requested pmid - Entity Extraction faild!"
    # prepare data
    authors = flatten_json(query.get("authorList", ""))
    data = {
        'pmid': pmid,
        'ner_data': metadata,
        'title':query.get("title", ""),
        'journal':query.get("journal", ""),
        'authors':authors,
        'url': query.get("doi", ""),
        'source':'Lit',
        'save_only': False
    }
    # api url for return
    if debug_mode:
        print('running in test mode')
        url = "http://127.0.0.1:8000/api/nermetadata/create_metadata/" # local debug
    else:
        url = "{}nermetadata/create_metadata/".format(parameters.ExternalMetadataPortal) # in production
    
    msg = requests.post(url, json = data)
    return msg.text

#%% extract raw text from pubmed online
@celery.task
def web_text_extract(pmid, link=None):
    print('text crawling requested')
    db = db_connection('structured_data')
    try:
        headers = {'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/39.0.2171.95 Safari/537.36'}
        # convert pmid to pmcid
        convert_api = "https://www.ncbi.nlm.nih.gov/pmc/utils/idconv/v1.0/?tool=my_tool&email=my_email@example.com&ids={}&versions=no&format=json"
        # link to full text from pumbed
        article_url = "https://www.ncbi.nlm.nih.gov/pmc/articles/{}/"
        # request for the full text
        req = requests.get(convert_api.format(pmid), headers=headers, timeout=4000).json()
        content_ok = False
        if 'pmcid' in req['records'][0]:
            # print('fetching ... ', req['records'][0]['pmcid'])
            pmcid = req['records'][0]['pmcid']
            html_content = requests.get(article_url.format(pmcid), headers=headers, timeout=5000)#.text
            content_ok = html_content.ok
        if not content_ok and link is not None: # extract all p tags from the direct link
            html_content = requests.get(link, headers=headers, timeout=5000)#.text
            soup = BeautifulSoup(html_content.content, 'html.parser')
            headings = {}
            article_content = {}
            article_content['title'] = []
            #sec_key = ""
            for tag in soup.find_all(["p", "h1", "h2", "h3", "h4", "h5"]): # iterate over all tags
                if tag: # check if tag is not empty
                    if tag.name == "h1": article_content['title'].append(tag.text.strip())
                    elif 'h2' == tag.name:
                        headings[tag.name] = tag.text.strip()
                        for h in ["h3", "h4", "h5"]: headings.pop(h, None)
                    elif 'h3' in tag.name:
                        headings[tag.name] = tag.text.strip()
                        for h in ["h4", "h5"]: headings.pop(h, None)
                    elif 'h4' in tag.name:
                        headings[tag.name] = tag.text.strip()
                        for h in ["h5"]: headings.pop(h, None)
                    elif 'h5' in tag.name:
                        headings[tag.name] = tag.text.strip()
                    else:
                        sec = []
                        for h in ["h2", "h3", "h4", "h5"]:
                            if h in headings:
                                sec.append(headings[h])
                        sec_key = ":".join(sec)
                        if "reference" in sec_key.lower():
                            continue
                        if sec_key in article_content:
                            article_content[sec_key].append(tag.text.strip())
                        else:
                            article_content[sec_key] = [tag.text.strip()]
            db.articles.update_one({'pmid':pmid}, {"$set":{'fulltext':article_content, 'date':datetime.now()}})
            return #article_pars
        elif not html_content.ok:
            db.articles.update_one({'pmid':pmid}, {"$set":{'fulltext':["fulltext is not available online!"], 'date':datetime.now()}})
            print("fulltext is not available online!")
            return
        # get full-text of the article using the PMC link
        print("get full-text of the article using the URL link")
        html_content = requests.get(article_url.format(pmcid), headers=headers, timeout=5000)#.text
        # Parse the html content
        # soup = BeautifulSoup(html_content, "lxml")
        soup = BeautifulSoup(html_content.content, 'html.parser')
        article_data = {}
        # get title
        title_element = soup.find("h1", class_="content-title")
        if title_element:
            article_data['title'] = [title_element.text.strip()]
        # get keywords
        keywords_element = soup.find("span", class_="kwd-text")
        if keywords_element:
            article_data['keywords'] = [keywords_element.text.strip()]
        # get the rest of the structured sections
        for title_section in soup.findAll("div", {"class": "tsec sec"}):
            h2 = title_section.find("h2")
            sub_sections = title_section.findAll("div", {"class": "sec", "id":re.compile("\w+$")})
            if sub_sections:
                for sub_section in sub_sections:
                    h3 = sub_section.find("h3")
                    sec_key = "article_section"
                    if h2:
                        sec_key = h2.text.lower().strip()
                        article_data[sec_key] = []
                    if h3:
                        sec_key += ":" + h3.text.lower().strip()
                        article_data[sec_key] = [h3.text.strip()]
                    sec_key = sec_key if len(sec_key) else "article_section"
                    for par in sub_section.find_all("p"):
                        if not par: continue
                        par_text = par.text.strip()
                        if par_text:
                            article_data[sec_key].append(par_text)
            else: # section without sub sections
                sec_key = "article_section"
                if h2:
                    sec_key = h2.text.lower().strip()
                sec_key = sec_key if len(sec_key) else "article_section"
                article_data[sec_key] = []
                for par in title_section.find_all("p"):
                    if not par: continue
                    par_text = par.text.strip()
                    if par_text:
                        article_data[sec_key].append(par_text)
        # write to db and return
        article_data = {k: v for k, v in article_data.items() if v} # remove empty
        db.articles.update_one({'pmid':pmid}, {"$set":{'fulltext':article_data, 'date':datetime.now()}})
    except Exception as e:
        db.articles.update_one({'pmid':pmid}, {"$set":{'fulltext':["fulltext is not extractable online!"], 'date':datetime.now()}})
        print("Text extraction failed! ({})".format(str(e)))
    return #{k: v for k, v in article_data.items() if v} # article_data

#%% make excel form out of the extracted metadat
import xlsxwriter
from heapq import nlargest

@celery.task
def excel_maker(pmid):

    # parameters
    workbook = xlsxwriter.Workbook("./excel_forms/{}.xlsx".format(re.sub('[^A-Za-z0-9.-]', '', pmid))) # modified to resolve the path issue
    worksheet = workbook.add_worksheet("Metadata_Form_NMO")
    
    worksheet.set_column("A:F", 35)
    worksheet.set_row(0, 35) 
    worksheet.set_row(1, 35) 
    worksheet.set_row(2, 30) 
    worksheet.set_row(3, 30) 

    # info
    msg = """Please fill in as much information as possible in this form.
    The final information listed in this form will be associated with your reconstructions.
    The prefilled information are extracted from your publication using a smart metadat extraction model.
    Pleae modify the entries in a way that best describes your experiment."""
    merge_format = workbook.add_format({
    'bold': 1,
    'align': 'center',
    'valign': 'vcenter',
    'font_size':15,
    'font_color':'white',
    'fg_color': 'green'})
    worksheet.merge_range('A1:F2', msg, merge_format)

    # info
    merge_format = workbook.add_format({
    'bold': 1,
    'border':1,
    'align': 'center',
    'valign': 'vcenter',
    'font_color':'white',
    'font_size':14,
    'fg_color': 'black',
    })

    worksheet.merge_range('A4:C4', "Reconstruction related information", merge_format)
    worksheet.merge_range('D4:F4', "Notes from NeuroMorpho.Org", workbook.add_format({'bold':1, 'border':1, 'align':'center', 'valign': 'vcenter', 'font_size':14, 'fg_color':'yellow'}))
    
    # Set up some formats to use.
    # constants
    green = workbook.add_format({'color': 'green', 'font_size':14,'valign': 'vcenter','align': 'center'})
    blue = workbook.add_format({'color': 'blue', 'font_size':14,'valign': 'vcenter','align': 'center'})
    red = workbook.add_format({'color': 'red', 'font_size':14,'valign': 'vcenter','align': 'center'})
    cell_format = workbook.add_format({'align': 'center',
                                   'valign': 'vcenter',
                                   'font_size':14,
                                   'fg_color':'ceylon',
                                   'border': 1})
    worksheet.merge_range('A3:F3', "", cell_format)
    worksheet.write_rich_string('A3', *[cell_format, 'Confidence of the extraction model for the extracted terms. ', green, 'Green', cell_format, ': above 90% confidence, ', blue, 'Blue', cell_format, ': 50%~90% confidence, ', red, 'Red', cell_format, ': below 50% confidence, blank/not reported please specify yourself'], cell_format)
    # worksheet.write_rich_string('A3', 'Extraction model confidence for terms. Green: above 90% confidence, Blue: 50%~90% confidence, Red: below 50% confidence, blank/not reported please specify yourself', cell_format)

    merge_format = workbook.add_format({
    'bold': 1,
    'border':1,
    'align': 'center',
    'valign': 'vcenter',
    'font_size':12,
    })

    # subject
    worksheet.merge_range('A5:A10', "Subject", merge_format)
    worksheet.write('B5', 'Species')
    worksheet.write('B6', 'Strain')
    worksheet.write('D6', 'Genotype or stock information can be reported here as well.')
    worksheet.write('B7', 'Gender')
    worksheet.write('B8', 'Developmental stage')
    worksheet.write('D8', 'Young, adult, old etc.')
    worksheet.write('B9', 'Age')
    worksheet.write('B10', 'Weight')

    # Anatomy
    worksheet.merge_range('A11:A12', "Anatomy", merge_format)
    worksheet.write('B11', 'Brain (sub)region of soma location')
    worksheet.write('D11', 'Please provide as many details as possible regarding the soma location, even if putative (if the soma was not reconstructed or exact anatomical boundaries were not traced). Examples: "Hippocampus>Dentate gyrus>Granule cell layer", "right optic lobe > right medulla", "neocortex > Somatosensory > S1 > barrel > layer 4", "Neocortex > motor cortex > bordering between M1 & M2", "Peripheral nervous system > dorsal thoracic ganglion", "hind brain > medulla > cochlear nucleus > anteroventral" , etc.')
    worksheet.write('B12', 'Celltype')
    worksheet.write('D12', 'Please provide as many details as possible regarding the neuron class, even if [putative] (assumed but not measured directly). Examples: "Principal cell > pyramidal cell > spiny star-pyramidal [glutamatergic]", "Interneurons > basket cell > fast-spiking, [PV+]"')
    
    # Experiment and Reconstruction
    worksheet.merge_range('A13:A21', "Experiment and Reconstruction", merge_format)
    worksheet.write('B13', 'Experimental protocol')
    worksheet.write('D13', '"In vivo" means that the neuron was labeled in the live animal (e.g. biocytin injection in anesthesized animal or genetic GFP construct); "in vitro" means that the neuron was labeld in the slice (e.g. biocytin injection after brain sectioning and patch clamp recording); "culture" refers to primary or organotypic conditions.')
    worksheet.write('B14', 'Experimental condition')
    worksheet.write('D14', 'Control vs. treatment')
    worksheet.write('B15', 'Stain')
    worksheet.write('B16', 'Slice thickness')
    worksheet.write('B17', 'Slicing direction')
    worksheet.write('D17', 'Example: transverse, horizontal, sagittal etc.')
    worksheet.write('B18', 'Tissue shrinkage')
    worksheet.write('D18', 'Known and corrected (values), or known and not corrected, or unknown.')
    worksheet.write('B19', 'Reconstruction software')
    worksheet.write('B20', 'Objective type')
    worksheet.write('D20', 'Dry, oil, water.')
    worksheet.write('B21', 'Objective magnification')
    
    # Data
    worksheet.merge_range('A22:A25', "Data", merge_format)
    worksheet.write('B22', 'Number of data files')
    worksheet.write('D22', 'The names associated with each reconstruction that you provide will be uploaded to the website as is. Due to database constraints, we request you to keep the file names short.')
    worksheet.write('B23', 'Numerical units')
    worksheet.write('D23', 'Are the X, Y, Z coordinates and branch thickness values expressed in microns or any other units (nanometers, millimeters, pixels, voxels, or optical section number)? If pixels, what are the pixel size and Z step in microns?')
    worksheet.write('B24', 'pixel size (if pixel)')
    worksheet.write('B25', 'Format of data files')
    worksheet.write('D25', 'Example: swc, asc, nrx, dat etc.')
    
    # Neuron Description
    worksheet.merge_range('A26:A30', "Neuron Description", merge_format)
    worksheet.write('B26', 'Soma')
    worksheet.write('D26', 'Which of the following neuronal compartments are included in your reconstruction files? And what portions do you consider complete, incomplete or moderately complete (due to slice sectioning, partial labeling, or resolution limits)?')
    worksheet.write('B27', 'Axon')
    worksheet.write('B28', 'Dendrites')
    worksheet.write('B29', 'Neurites')
    worksheet.write('B30', 'Processes')
    
    # Morphological Attributes
    worksheet.merge_range('A31:A33', "Morphological Attributes", merge_format)
    worksheet.write('B31', 'dimension')
    worksheet.write('D31', 'Which of the following measurements do you consider reliable in your reconstruction files?')
    worksheet.write('B32', 'angles')
    worksheet.write('B33', 'diameter')
    
    # Contributor related information
    worksheet.merge_range('A34:A37', "Contributor related information", merge_format)
    worksheet.write('B34', 'Archive Name')
    worksheet.write('D34', 'The reconstructions will be archived under this name (ex: Jacobs archive, Markram archive, Yuste archive etc)')
    worksheet.write('B35', 'Lab Name')
    worksheet.write('D35', 'Name of the laboratory that should be acknowledged in NeuroMorpho.Org’s “Acknowledgements” page: http://neuromorpho.org/neuroMorpho/acknowl.jsp')
    worksheet.write('B36', 'Institute')
    worksheet.write('B37', 'Address')
    
    # Notes
    worksheet.write('A38', 'Any Additional Notes', merge_format)
    worksheet.write('B38', 'notes')
    worksheet.write('D38', 'Any additional notes about the reconstructions that you would like to include.')



    # read the extracted information from the text and put it into the form
    # db2 = db_connection('structured_data')
    # metadata = db2.articles.find_one({'pmid':pmid}, {'_id': False})['metadata']
    # read the extracted information from the text and put it into the form
    db2 = db_connection('structured_data')
    query = db2.articles.find_one({'pmid':pmid}, {'_id': False})
    if query and 'metadata' in query:
        metadata = query['metadata']
    else:
        return "metadata does not exist for the requested pmid - excel maker abrupted"

    if len(metadata):
        # func
        def writer(tmp, loc): # writer function
            term_sug = []
            for key in nlargest(3, tmp, key=tmp.get):
                if tmp[key] >= .8:
                    term_sug.append(workbook.add_format({'color': 'green'}))
                elif tmp[key] >= .5:
                    term_sug.append(workbook.add_format({'color': 'blue'}))
                else:
                    term_sug.append(workbook.add_format({'color': 'red',}))
                term_sug.append(key)
                term_sug.append(', ') # space between terms
            if len(term_sug) > 3:
                term_sug.pop()
                worksheet.write_rich_string(loc, *term_sug)
            elif len(term_sug) == 3:
                worksheet.write(loc, term_sug[1], term_sug[0])
            else:
                print('not reported')
                worksheet.write(loc, "Not reported", workbook.add_format({'color': 'gray',}))

        
        # Metadata
        # Species
        tmp, loc = metadata['SPE'], 'C5'
        writer(tmp, loc)
        
        # Strain
        tmp, loc = metadata['STR'], 'C6'
        writer(tmp, loc)
        
        # Gender
        tmp, loc = metadata['GEN'], 'C7'
        writer(tmp, loc)
        
        # Development stage
        tmp, loc = metadata['DEV'], 'C8'
        writer(tmp, loc)
        
        # Age and Weight
        # worksheet.write('C9', '?')
        # worksheet.write('C10', '?')

        # Brain Region
        tmp, loc = metadata['REG'], 'C11'
        writer(tmp, loc)
        
        # Celltype
        tmp, loc = metadata['CEL'], 'C12'
        writer(tmp, loc)
        
        # Protocol
        tmp, loc = metadata['PRO'], 'C13'
        writer(tmp, loc)

        # Experimental condition
        tmp, loc = metadata['EXP'], 'C14'
        writer(tmp, loc)
        
        # Stain
        tmp, loc = metadata['STA'], 'C15'
        writer(tmp, loc)
        
        # Slicing thickness
        # worksheet.write('C16', '?')

        # Slicing direction
        tmp, loc = metadata['SLI'], 'C17'
        writer(tmp, loc)

        # Software
        tmp, loc = metadata['REC'], 'C19'
        writer(tmp, loc)

        # Objective
        tmp, loc = metadata['OBJ'], 'C20'
        writer(tmp, loc)



    print('writing to file...')
    workbook.close()

# test run
# excel_maker('202020')

#%% Machine Learning model
# imports
import urllib.parse

import torch
import pickle
import math
import numpy as np
from transformers import BertTokenizer
from transformers import BertForTokenClassification
from transformers import BertModel
import nltk
# nltk.data.path.append("/scratch/kbijari/nltk/")
import pandas as pd
import re
from collections import Counter
from string import digits
remove_digits = str.maketrans('', '', digits)
import nltk
# local imports
import sys
# sys.path.append('../')
from normalizer import word_list_normalize
from auxiliary import matchlib, remove_values_from_list, update_progress, bruteforce_ner
from auxiliary import decodeKey, encodeKey
from auxiliary import resolve_roman
# resolve abbreviations
from auxiliary import resolve_abbreviation
from auxiliary import resolve_abbreviation2
# resolve plurals
import inflect
pl = inflect.engine()
# our cases that need to stay fix
with open('./saved-models/neuro-fix-names.txt') as f:
    neuro_names = f.read()
neuro_fix = [n.lower() for n in neuro_names.splitlines()]
for ft in list(dict.fromkeys(neuro_fix)):
    pl.defnoun(ft, ft)

# extract metadata from the text of the articles
@celery.task
def metadata_extract(pmid, article_text=[]):
    # time.sleep(5) # test
    # saved model locations
    loc = './saved-models/bert100ep'
    home_dir = '.'
    # ignore term lists
    ignore_list = ['nr','not', 'applicable', 'nan', 'reported', 'not reported', 'not applicable',
               'of', 'with', '-', ':',
               '[sep]', '[unk]', '[UNK]', '[cls]', 'unk',]
    bert_ignore_list = ['nr','not', 'applicable', 'nan', 'reported', 'not reported', 'not applicable',
               'to', 'of', 'with',
               '-', ':', '.', ',',
               '[sep]', '[unk]', '[UNK]', '[cls]', 'unk',]
    keys = ['SPE','STR','GEN','DEV','REG', 'CEL','EXP','STA','SLI','PRO','OBJ','REC','AGE','OTH',]
    # read terms and put them in a list for BF-NER search
    db1 = db_connection('nmo_terms')
    results = db1.term_db.find({'bf-search':True})
    nmo_terms = dict()
    for r in results:
        nmo_terms[decodeKey(r['term'])] = r['category']
    if len(nmo_terms) == 0:
        print("could not get terms from the database")
    # local db of the terms
    local_term_db = {}
    for t in list(db1.term_db.find({}, {"_id":0})):
        local_term_db[t['term']] = t
    # local db of the maps
    local_term_maps = {}
    for t in list(db1.term_maps.find({}, {"_id":0})):
        local_term_maps[(t['term'], t['category'])] = t
    # functions
    # join terms
    def term_join(term_list):
        joined = ""
        f = False
        for idx, t in enumerate(term_list):
            if idx == 0:
                joined += t
            elif len(t) == 1 and not (t.isalpha() or t.isdigit()):
                joined += t
                f = True
            elif f:
                joined += t
                f = False
            else:
                joined += ' {}'.format(t)
        return joined
    # polish list from unwanted chars
    def polish(term_list, category):
        gender =  [] # add genders
        if category == 'GEN':
            if 'M/F' in term_list: gender.append('male/female')
            for g in ['Male', 'Female', 'Hermaphrodite']:
                if g[0] in term_list or g[0].lower() in term_list:
                    gender.append(g.lower())
        tmp = [str(t).lower() for t in term_list] + gender # lower
        if category not in ['EXP', 'STR', 'AGE', 'STA', 'REG', 'CEL', 'REC', 'ORI']: # remove digits from terms
            tmp = [t.translate(remove_digits) for t in tmp]
        tmp = [t for t in tmp if len(t)] # remove empty strings
        if not len(tmp): # return if empty
            return tmp
        tmp_ = []
        for term in tmp:
            term_words = term.split()
            last = term_words.pop()
            if pl.singular_noun(last):
                last = pl.singular_noun(last)
            term_words.append(last)
            tmp_.append(term_join(term_words))
        tmp = tmp_ # singularized and remove spaced terms
        tmp = list(dict.fromkeys(tmp)) # keep uniq and preserve order
        tmp = [t for t in tmp if len(t) > 1 and t not in ignore_list]
        return tmp
    def corrections(string):
        if string.lower().find('method') != -1:
            return 'materials and methods'
        string = re.sub(r'\d+', '', string).replace('.', '')
        # string = re.sub(r'.', '', string)
        return string.strip()
    def freq_sig(x):
        if x <= 0: return 0
        return 0.15*(1 - math.exp(-0.3*x))
    def goodness_sig(x):
        if x <= 0: return 0
        return 0.25*(1 - math.exp(-4*x))
    def stat_sig(x):
        if x <= 0: return 0
        return 0.25*(1 - math.exp(-4*x))
    def section_sig(section):
        if type(section) != str:
            return 0
        if 'animal' in section.lower():
            return .35
        if 'caption' in section.lower():
            return .33
        if 'figure' in section.lower():
            return .33
        if 'abstract' in section.lower():
            return .30
        if 'summary' in section.lower():
            return .31
        if 'title' in section.lower():
            return .29
        if 'method' in section.lower():
            return .24
        elif 'result' in section.lower():
            return .22
        elif 'discussion' in section.lower():
            return .21
        else:
            return .15
    ### new method to make polished entities
    polished_entities = {c:[] for c in keys}
    for k, v in local_term_db.items():
        if v['category'] in keys:
            polished_entities[v['category']].append(v['term_original'])
    polished_entities['OTH'] = []
    polished_entities['AGE'] = []
    ### modifications
    if "Wildtype" in polished_entities['STR']:
        polished_entities['STR'].remove('Wildtype')
    if "granule cell layer" in polished_entities['REG']:
        polished_entities['REG'].remove('granule cell layer')
    polished_entities['GEN'] = ['male', 'female', 'hermaphrodite', 'male/female']
    
    # machine learning model variables
    keys = [
    'SPE',
    'STR',
    'GEN',
    'AGE',
    'DEV',
    'REG',
    'CEL',
    'EXP',
    'STA',
    'SLI',
    'PRO',
    'OBJ',
    'REC',
    'OTH',
    ]
    # hits = 0
    # total_cnt = 0
    # laod bert & vars
    with open(loc + '/vars.pickle', 'rb') as fp:
        tag_values = pickle.load(fp)
    # tokenizer & model
    tokenizer = BertTokenizer.from_pretrained(loc, do_lower_case=False, truncation=True, max_len=512)
    model = BertForTokenClassification.from_pretrained(loc)
    model.eval() # put the model in evaluation mode
    # sentence classifier
    sent_vectorizer = BertModel.from_pretrained('./saved-models/NEURONER25/') # Neuro
    sent_ranker = pickle.load(open('./saved-models/sentence_rank.sav', 'rb'))
    def ranker(sent, entity=None, category=None):
        # return if sentence contains citation
        pattern = "[A-Za-z]{2,40}\s\d{4}\)|\set\sal.\W\s\d{4}(;|,|\s|\w)*|\[\d+((,|-|\s)*\d+)*\]"
        if category not in ['REC']:
            if len(re.findall(pattern, sent)) > 0:
                return 0.0
        tok = tokenizer.tokenize(sent) # tokenize
        tok = ['[CLS]'] + tok + ['[SEP]'] # add bounderies
        if len(tok) > 512:
            padded_tokens = tok[0:512] # +['[PAD]' for _ in range(512 - len(tok))] # pad senteces to a same length
        else:        
            padded_tokens = tok +['[PAD]' for _ in range(512 - len(tok))] # pad senteces to a same length
        token_ids_tmp = tokenizer.convert_tokens_to_ids(padded_tokens) # token to id
        token_ids = torch.tensor(token_ids_tmp).unsqueeze(0) # make tensor
        with torch.no_grad(): # sentence to embeding
            out = sent_vectorizer(token_ids)
        sent_rank = sent_ranker.predict(out[0][:,0,:].numpy())[0] * sent_ranker.predict_proba(out[0][:,0,:].numpy())[0][1]  # probability of the sentence being informative 1 otherwise 0
        return sent_rank

    # read the extracted information from the text and put it into the form
    db2 = db_connection('structured_data')
    if len(article_text) == 0: # if full text is not sent, check for it in the database
        query = db2.articles.find_one({'pmid':pmid}, {'_id': False})
        if query and 'fulltext' in query:
            article = query['fulltext']
        else:
            return "fulltext does not exist for the requested extraction"
        # content = [par for lst in list(article.values()) for par in lst]
    else:
        article = article_text

    # resolve abbreviations
    article = resolve_abbreviation2(article)
    # content = resolve_abbreviation(content)
    # entities = {k:[] for k in keys}
    # entities_sec = {k:[] for k in keys}
    ranked_entities = {k:[] for k in keys}
    # =============================================================================
    #  BERT
    # =============================================================================
    ignore_bert = 0
    section_index = 0
    if not ignore_bert: print('BERT...')
    # for par_num, paragraph in enumerate(content):
        # update_progress(par_num, len(content))
    for section, paragraphs in article.items():
        update_progress(section_index, len(article), text=pmid)
        section_index += 1
        if ignore_bert: continue ### ignore BERT
        for paragraph in paragraphs:
            if not len(paragraph): continue # skip the empty ones
            for sent in nltk.sent_tokenize(paragraph):
                sent_ents = bruteforce_ner(sent, keys, nmo_terms) # extract using BF
                tokenized_sentence = tokenizer.encode(sent)
                tokens, label_indices = [], []
                for itr in range(math.ceil(len(tokenized_sentence)/512)): # make sure nothing is missed
                    tkn_snt = tokenized_sentence[itr*512: (itr+1)*512]
                    input_ids = torch.tensor([tkn_snt]).cpu()
                    with torch.no_grad():
                        output = model(input_ids)
                    label_indices_tmp = np.argmax(output[0].to('cpu').numpy(), axis=2)
                    label_indices.extend(label_indices_tmp)
                    # join bpe split tokens
                    tokens_tmp = tokenizer.convert_ids_to_tokens(input_ids.to('cpu').numpy()[0])
                    tokens.extend(tokens_tmp)
                new_tokens, new_labels = [], []
                for token, label_idx in zip(tokens, label_indices[0]):
                    if token.startswith("##"):
                        new_tokens[-1] = new_tokens[-1] + token[2:]
                    else:
                        new_labels.append(tag_values[label_idx])
                        new_tokens.append(token)
                # put the named entities together
                tmp_token, tmp_label = [], ''
                ents = []
                for token, label in zip(new_tokens, new_labels):
                    if 'B-' in label and not tmp_label:
                        tmp_token.append(token)
                        tmp_label = label.replace('B-', '')
                    elif 'B-' in label and len(tmp_label) > 0:
                        if term_join(tmp_token) not in sent_ents[tmp_label]:#
                            sent_ents[tmp_label].append(term_join(tmp_token))
                        tmp_token = [token]
                        tmp_label = label.replace('B-', '')
                    elif 'I-' in label and 0 == len(tmp_label):
                        key = label.replace('I-', '')
                        if term_join(token) not in sent_ents[key]:##
                            sent_ents[key].append(token)
                    elif 'I-' in label and len(tmp_label) > 0:
                        tmp_token.append(token)
                    elif 'O' == label and len(tmp_label): # add the leftovers
                        if term_join(tmp_token) not in sent_ents[tmp_label]:###
                            sent_ents[tmp_label].append(term_join(tmp_token))
                        tmp_token, tmp_label = [], ''
                # add
                for cat, ents in sent_ents.items():
                    for ent in ents:
                        sent_rank = ranker(sent, ent, cat)
                        section_rank = section_sig(section)
                        ranked_entities[cat].append((ent, sent_rank, section_rank, sent))
    # rank entities
    final_ranked_entities = {k:{} for k in keys}
    for key, value in ranked_entities.items():
        ent_list = []
        ent_list_sim = {}
        # ents_per_cat = polish([str(e[0]).lower() for e in value], key) # original
        ents_per_cat = auxiliary.polish([str(e[0]).lower() for e in value], key, ignore_list) # new
        for ent in ents_per_cat:
            match = matchlib(ent, polished_entities[key], local_term_maps, key)
            ent_list.extend(list(match))
            ent_list_sim.update(match)
        uniq_ents = {e:0 for e in list(dict.fromkeys(ent_list))}     # uniq list of ents
        uniq_ents_sum = {e:0 for e in list(dict.fromkeys(ent_list))} # uniq list of ents
        uniq_ents_sec = {e:0 for e in list(dict.fromkeys(ent_list))} # uniq list of ents
        uniq_ents_sen = {e:[0, "-"] for e in list(dict.fromkeys(ent_list))} # uniq list of ents
        for e in value:
            # k = polish([e[0]], key) # original
            k = auxiliary.polish([e[0]], key, ignore_list) # new
            if len(k):
                k = k[0]
                kmatch = matchlib(k, polished_entities[key], local_term_maps, key)
                for km in list(kmatch):
                    uniq_ents[km]     = max(uniq_ents[km], e[1])
                    uniq_ents_sum[km] = uniq_ents_sum[km] + e[1]
                    uniq_ents_sec[km] = max(uniq_ents_sec[km], e[2])
                    if e[1] * e[2] >= uniq_ents_sen[km][0]:
                        uniq_ents_sen[km] = [e[1] + e[2], e[3]]
        # polish the list
        for ent, prb in uniq_ents.items():
            uniq_ents[ent] =  goodness_sig(prb) + freq_sig(uniq_ents_sum[ent]) + uniq_ents_sec[ent]
            if ent.lower() in local_term_db.keys(): # new
                uniq_ents[ent] += stat_sig(local_term_db[ent.lower()]['term_abu'])
        # apply levels for brain region and celltype
        if key in ['CEL', 'REG']:
            uniq_ent_keys = list(uniq_ents)
            for ent in uniq_ent_keys:
                ent_lower = ent.lower()
                if ent_lower in local_term_db:
                    pk = local_term_db[ent_lower]['pk']
                    if len(pk) and local_term_db[ent_lower]['category'] == key:
                        max_val = max(pk.values()) * 0.70
                        for p, v in pk.items():
                            if v >= max_val:
                                new_ent = p +'-->>' + ent_lower
                                uniq_ents[new_ent] = uniq_ents[ent]
                                uniq_ents_sen[new_ent] = uniq_ents_sen[ent]
                        uniq_ents.pop(ent)
            # merge repeated levels
            to_pop = []
            for k1 in uniq_ents:
                for k2 in uniq_ents:
                    if k1 == k2: continue
                    if k1 in k2 and k1.count('-->>') < k2.count('-->>'):
                        if k1 not in to_pop:
                            to_pop.append(k1)
                        uniq_ents[k2] = min(1.0, max(uniq_ents[k1], uniq_ents[k2])+0.10*min(uniq_ents[k1], uniq_ents[k2]))
            for var in to_pop:
                uniq_ents.pop(var)
        # add sentences to the ranking report
        uniq_ents2 = {}
        for eq, eqp in uniq_ents.items():
            uniq_ents2[eq] = [eqp, uniq_ents_sen[eq][1]]
        final_ranked_entities[key] = uniq_ents2
        # sort and add to the dict
#        sorted_uniq_ents = dict(sorted(uniq_ents.items(), key=lambda item:item[1], reverse=True))
#        final_ranked_entities[key] =  sorted_uniq_ents # original
    # write to the database
    db2.articles.update_one({'pmid':pmid}, {"$set":{'metadata':final_ranked_entities, 'date':datetime.now()}})
    return "Metadata extracted and saved"