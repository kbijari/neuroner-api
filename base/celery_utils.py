

def init_celery(celery, app):
    celery.conf.update(app.config)
    TaskBase = celery.Task
    class ContextTask(TaskBase):
        def __call__(self, *args, **kwds):
            with app.app_context():
                return TaskBase.__call__(self, *args, **kwds)
    celery.Task = ContextTask