from email.policy import default
import imp
import os
import flask
import time
import parameters
import json
from flask import Blueprint
from celery import Celery
from celery import chain
from flask import json
from flask_restful import Resource, Api, reqparse
from flask import request, jsonify
from flask_restful import reqparse
from pymongo import MongoClient
from bson import json_util
import urllib.parse
from flask import current_app
from datetime import datetime
from pytz import timezone
from .tasks import ml_prepare, web_text_extract
from .tasks import metadata_extract
from .tasks import excel_maker
from .tasks import callback_api


bp = Blueprint("api_core", __name__)
api = Api(bp)


### local functions

# setup database connection
def db_connection():
    # username = urllib.parse.quote_plus('root')
    # password = urllib.parse.quote_plus('metadata@nmo2006')
    username = urllib.parse.quote_plus(os.environ.get('DB_USER', 'root'))
    password = urllib.parse.quote_plus(os.environ.get('DB_PASS', 'metadata@nmo2006'))
    db_host = os.environ.get('DB_HOST', 'cng-nmo-dev6.orc.gmu.edu')
    mongo_client = MongoClient('mongodb://%s:%s@%s:27017' % (username, password, db_host))
    db = mongo_client['structured_data'] # database
    return db
### test writing in the db
# db.articles.insert_one({'pmid':'124', 'text':'second test'})

# request data extraction
def extract_structured_data(pmid):
    db = db_connection()
    # web_text_extract.apply_async((pmid), link=metadata_extract.s()) # link to extract metadata when fulltext is ready
    # chain(web_text_extract.s(pmid), metadata_extract.si(pmid), excel_maker.si(pmid))() # extract the full text and then run the ML metadata extraction
    chain(web_text_extract.s(pmid), metadata_extract.si(pmid))() # excel maker removed from the chain
    # mark in the db
    db.articles.insert_one({'pmid':pmid, 'fulltext':['Pending'], 'date':datetime.now(timezone('EST'))})
    return

### class resources

# show status of the api/db/model...
class Status(Resource):
    def get(self):
        status = "O.K."
        try:
            db = db_connection()
            article_count = db.articles.count_documents({})
            db_msg = "{} structured article(s) are available in the database!".format(article_count)
        except Exception as e:
            db_msg = "DB connection failed"
        if current_app.config['TESTING']:
            running_mode = "Test"
        else:
            running_mode = "Production"
        return {'api_status':status, 'db_status':db_msg, 'ML_status':status, 'Running_mode':running_mode}
# add endpoint
api.add_resource(Status, '/status', '/')  # return api status

# list of recent articles
class Articles(Resource):
    """ return a number of recent added articles
    """
    def get(self):
        try:
            db = db_connection()
            max_retrive = 25
            data = []
            articles = db.articles.find({}, {'_id': False}).limit(max_retrive)
            for article in articles:
                tmp = {}
                for key, val in article.items():
                    if len(str(val))>50:
                        tmp[key] = str(val)[:150] + '...'
                    else:
                        tmp[key] = str(val)
                data.append(tmp)
                # data.append(json.loads(json_util.dumps(a)))
            return {'message':'Latest {} added articles!'.format(max_retrive), 'data':data}
        except Exception as e:
            message = "Error while retriving articles ({})!".format(str(e))
            return {'message': message}
# end point
api.add_resource(Articles, '/articles')  # '/ entry point for articles

# return or request articles by pmid
class ArticleByPMID(Resource):
    """ return an article by PMID
    """
    def get(self, pmid):
        try:
            db = db_connection()
            metadata = []
            article = db.articles.find_one({'pmid':pmid}, {'_id': False})
            if article:
                return {'state': 'Ready', 'message':'Found!', 'data':json.loads(json_util.dumps(article))}
            else:
                extract_structured_data(pmid)
                return {'state': 'Pending', 'message':'This PMID is not Available in the Database! Metadata extraction is requested.'}
        except Exception as e:
            message = "Error while retriving ({})!".format(str(e))
            return {'message': message}
# end point
api.add_resource(ArticleByPMID, '/articles/pmid/<pmid>', endpoint='article_by_pmid')


# extract metadata for a pmid
class Extract(Resource):
    def get(self, pmid):
        metadata_extract.delay(pmid)
        return {'message':'metadata extraction requested!'}
# add endpoint
api.add_resource(Extract, '/extract/pmid/<pmid>', endpoint='extract') 

# extract metadata for a pmid
class PortalExtract(Resource):
    def post(self):
        db = db_connection()
        json_data = request.get_json(force=True)
        print(json_data)
        if 'pmid' in json_data.keys():
            pmid = str(json_data['pmid'])
            url = json_data.get('url', '')
            save_only = json_data.get('save_only', False)
            db.articles.update_one({'pmid':pmid}, {"$set":{'fulltext':['Pending'], 'date':datetime.now(timezone('EST'))}}, upsert=True)
            # if pmid.isnumeric(): # extract info from pubmed and return
            chain(web_text_extract.s(pmid, url), metadata_extract.si(pmid), callback_api.si(pmid, save_only=save_only, debug_mode=current_app.config['TESTING']))() # extract the full text and then run the ML metadata extraction
            return {'message':'Request recieved!'}
        else:
            return {'message':'Request is missing PMID/DOI!'}
# add endpoint
api.add_resource(PortalExtract, '/portal-extract', endpoint='portal-extract') 

# collect metadata for an article - calls from literature portal
class LitermateCollect(Resource):
    def post(self):
        try:
            db = db_connection()
            json_data = request.get_json(force=True)
            json_data['date'] = datetime.now(timezone('EST')) # time tag
            json_data['source'] = "litermate" # sign where the data is coming from
            if 'pmid' in json_data:
                if json_data['pmid']:
                    pmid = json_data['pmid']
                else:
                    pmid = json_data['doi']
                del json_data['pmid']
                db.articles.update_one({'pmid':pmid}, {"$set":json_data}, upsert=True)
            else:
                db.articles.insert_one(json_data)
            chain(ml_prepare.s(pmid, current_app.config['TESTING']))()
            return {'status':'O.K.', 'message':'Request recieved and data is saved!'}
        except Exception as e:
            return {'status':'Error', 'message':'An error occured while recieving the data ({})!'.format(str(e))}

# add endpoint
api.add_resource(LitermateCollect, '/litermate-collect', endpoint='litermate-collect')

# write metadata to excel
class Export(Resource):
    def get(self, pmid):
        excel_maker.delay(pmid)
        return {'message':'Excel file is written to file!'}
# add endpoint
api.add_resource(Export, '/export/pmid/<pmid>', endpoint='export')