#!/bin/sh

# Stop all containers
docker stop $(docker ps -a -q)

# remove all containers
docker rm $(docker ps -a -q)

# remove all unused reseources
docker system prune -a

# remove all images
docker rmi $(docker images -a -q)

# remove all volumes
docker volume prune
