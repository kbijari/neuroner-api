# NeuroNER API

An open-source project for aided annotation of neuronal reconstructions in neuroscience literature. Developed for NeuroMorpho.org and succesfully applied to cng-nmo-meta.orc.gmu.edu. To get more information about the metadata portal please check out here: [metadata-portal] and [metadata-portal-paper]

<!-- TABLE OF CONTENTS -->
## Table of Contents

* [About the Project](#about-the-project)
  * [Built With](#built-with)
* [Getting Started](#getting-started)
  * [Prerequisites](#prerequisites)
  * [Installation](#installation)
* [Dockerization](#dockerization)
* [API Usage](#api-usage)
* [Speed-bumps](#speed-bumps)
* [Contributing](#contributing)
* [License](#license)
* [Contact](#contact)
* [Acknowledgements](#acknowledgements)



<!-- ABOUT THE PROJECT -->
## About The Project

Shared data must be annotated with appropriate descriptive metadata to maximize the impact and discovery of research. Here we have developed a model that automatically detects and extracts neuroscience metadata from publications using deep learning. This project is developed to work in concert with the metadata portal as a part of the metadata management scheme for NeuroMorpho.Org. This project is fully open-sourced, and the development may also be extended to other research projects.

Key features:
* Smart
* Open-source
* Easy to integrate

### Built With:
* [Python](https://www.python.org/)
* [Flask](https://flask.palletsprojects.com/en/2.1.x/)
* [JQuery](https://jquery.com)
* [MongoDB](https://www.mongodb.com/)
* [Celery](https://github.com/celery/celery)
* [Transformers](https://huggingface.co/docs/transformers/index)

<!-- GETTING STARTED -->
## Getting Started

This is an example of how you may set up the metadata portal locally to integrate it for your personal use. Please clone or download the source code and follow these example steps.

### Prerequisites

The following steps and all packages are installed and tested on **Ubuntu 20.04** and **Centos 7** using **Python 3.8**. _Please note that some specific details may be different if you are using a different operating system or versions_.

### Installation

Please follow the installation guides step by step.

#### Install and configure MongoDB
Simply use the following command to install MongoDB on Linux:
```sh
$ sudo apt install mongodb
```
For more information follow along with the instructions detailed on [MongoDB] page and install it on your operating system.

Use the following command to create a user and password. Then place your passowrds in the credential.py located in the root folder.
```sh
$ mongosh --port 27017  --authenticationDatabase "admin" -u "myUserAdmin" -p
```
Visit here for more details: https://www.mongodb.com/docs/manual/tutorial/create-users/

###### Install MongoDB using docker
If you don't have docker and docker-compose installed, please follow https://docs.docker.com/install/ for docker installation and refer to https://docs.docker.com/compose/install/ for docker compose installation.

Once you have them installed, make the following `docker-compose.yml` file:
```sh
version: '3.5'

services:

  mongo:
    image: mongo:latest
    container_name: 'nmomongo'
    restart: always
    environment:
      MONGO_INITDB_ROOT_USERNAME: root # change this to your desired user
      MONGO_INITDB_ROOT_PASSWORD: metadata@nmo<year> # change this to your desired password
    ports:
      - 27017:27017
    volumes:
      - ~/nmo-terms-db:/data/db

```

Then run it using `docker-compose up -d`

To check MongoDB container is running or not use `docker ps`, you should see the following:
```sh
CONTAINER ID   IMAGE          COMMAND                  CREATED         STATUS        PORTS                      NAMES
a3eeb134b848   mongo:latest   "docker-entrypoint.s…"   12 months ago   Up 5 months   0.0.0.0:27017->27017/tcp   nmomongo
```

Connect to mongodb:
```
mongo admin -u root -p rootpassword
```
Please note that monog shell should be installed in this case. To do so follow instructions on https://docs.mongodb.com/manual/installation/

##### Move/Transfer the data from MongoDB to a new virtual server
Create a database dump
```sh
$ docker exec -i <container_name> /usr/bin/mongodump --username <username> --password <password> --authenticationDatabase admin --db <database_name> --out /dump
```
Move them out of the container.
```sh
$ docker cp <container_name>:/dump ~/Downloads/dump
```

On a new server, just like before, we should first make the dumped database files available within the docker container.
``` sh
docker cp ~/Downloads/dump <container_name>:/dump
```
Now that the dump files are available within the container, run the following command to have everything be imported.that the dump files are available within the container, run the following command to have everything be imported.
```sh
$ docker exec -i <container_name> /usr/bin/mongorestore --username <username> --password <password> --authenticationDatabase admin /dump/<database_name>

```


#### Install Redis

```sh
curl -fsSL https://packages.redis.io/gpg | sudo gpg --dearmor -o /usr/share/keyrings/redis-archive-keyring.gpg

echo "deb [signed-by=/usr/share/keyrings/redis-archive-keyring.gpg] https://packages.redis.io/deb $(lsb_release -cs) main" | sudo tee /etc/apt/sources.list.d/redis.list

sudo apt-get update
sudo apt-get install redis
```

#### Install python3 and other required packages
1. Download and install Python and pip for your prefered operating system. (for more details please vist: https://www.python.org/downloads/)

```sh
# centos 7 or ubuntu:
$ sudo apt-get update
$ sudo apt-get install python3
$ sudo apt-get install python3-pip
```
2. It’s highly recommended to install `Virtualenv` that creates new isolated environments to isolates your Python files on a per-project basis. That will ensure that any modifications made to your project won’t affect others you’re developing. The interesting part is that you can create virtual environments with different python versions, with each environment having its own set of packages.
```sh
$ pip3 install virtualenv
Collecting virtualenv
  Downloading virtualenv-x.x.x-pyx.pyx-none-any.whl (1.8MB)
  100% |████████████████████████████████| 1.8MB 367kB/s
Installing collected packages: virtualenv
Successfully installed virtualenv-x.x.x
```
3. Make your virtual environment
```sh
$ python3.8 -m venv ner
```
4. Activate the virtual environment
```sh
$ source ./ner/bin/activate
>> (ner) $
```
5. Install the required Python packages (locate the command line prompt to the source code directory)
```sh
(ner) $ pip install -r requirements.txt --no-index --find-links file:///tmp/packages
```
6. Run and check if the NeuroNER API produces any errors
```sh
(ner) $ python3 manage.py
```

#### Running components

NeuroNER API consist of two running modules. The first one is the API core and the latter is worker that waits for tasks to fulfill. Therefore, to have the API fully working, the two modules should be running at the same time.

##### Running the core module
As exampled above, to run the core module simply run the manage.py file with your python prompt.
```sh
(ner) $ python3 manage.py
```

##### Running the worker module
The worker module should run through Celery. Use the command below to execute it.
```sh
(ner) $ celery -A celery_worker.celery worker --loglevel=INFO
```

If the two modules are running without any errors then the installation is done. These services could be used in production in any way you desire.

## Run as a service
Please follow the steps below to run the code as a fully functioned service on a webserver. (Please make sure to change name/group accordingly to the user/group on your operating system)

### API core service
Make a service file in systemd using `sudo nano /etc/systemd/system/nerproject.service` and paste the following lines.
```
[Unit]
Description=Service instance for ner-project
After=network.target

[Service]
User=kbijari
Group=nginx
WorkingDirectory=/home/kbijari/project/neuroner-api
ExecStart=/home/kbijari/project/vner/bin/gunicorn --workers 2 --bind unix:ner.sock run:app

[Install]
WantedBy=multi-user.target
```
Run the service using the commands below
```sh
sudo systemctl start nerproject.service
sudo systemctl status nerproject.service

```

### Celery worker as a service
Make a config file for Celery using the `sudo nano /etc/celery` and paste the following lines:
```
# Name of nodes to start
# here we have a single node
#CELERYD_NODES="w1"
# or we could have three nodes:
CELERYD_NODES="w1"

# Absolute or relative path to the 'celery' command:
CELERY_BIN="/home/kbijari/project/vner/bin/celery"


# App instance to use
# comment out this line if you don't use an app
#CELERY_APP="metadata"
# or fully qualified:
#CELERY_APP="proj.tasks:app"
CELERY_APP="celery_worker.celery"

# How to call manage.py
CELERYD_MULTI="multi"

# Extra command-line arguments to the worker
CELERYD_OPTS="--time-limit=18000 --concurrency=1"

# - %n will be replaced with the first part of the nodename.
# - %I will be replaced with the current child process index
#   and is important when using the prefork pool to avoid race conditions.
CELERYD_PID_FILE="/var/run/celery/%n.pid"
CELERYD_LOG_FILE="/var/log/celery/%n%I.log"
CELERYD_LOG_LEVEL="INFO"
```

Save and then make a service file in systemd using `sudo nano /etc/systemd/system/celery.service` and paste the following lines.
```
[Unit]
Description=Celery Service
After=network.target

[Service]
Type=forking
PermissionsStartOnly=True
User=kbijari
Group=kbijari
RuntimeDirectory=celery
RuntimeDirectoryMode=0775
EnvironmentFile=/etc/celery
WorkingDirectory=/home/kbijari/project/neuroner-api
ExecStart=/bin/sh -c '${CELERY_BIN} -A $CELERY_APP multi start $CELERYD_NODES \
    --pidfile=${CELERYD_PID_FILE} --logfile=${CELERYD_LOG_FILE} \
    --loglevel="${CELERYD_LOG_LEVEL}" $CELERYD_OPTS'
ExecStop=/bin/sh -c '${CELERY_BIN} multi stopwait $CELERYD_NODES \
    --pidfile=${CELERYD_PID_FILE} --loglevel="${CELERYD_LOG_LEVEL}"'
ExecReload=/bin/sh -c '${CELERY_BIN} -A $CELERY_APP multi restart $CELERYD_NODES \
    --pidfile=${CELERYD_PID_FILE} --logfile=${CELERYD_LOG_FILE} \
    --loglevel="${CELERYD_LOG_LEVEL}" $CELERYD_OPTS'
Restart=always

[Install]
WantedBy=multi-user.target

```
Run the service usign the commands below
```sh
sudo systemctl start celery.service
sudo systemctl status celery.service
```

Once both services are running properly you can set up Nginx to run the API core and handle the coming requests.

### Nginx setup to handle the API service

Install
```sh
sudo apt update
sudo apt install nginx
```

add a new server in the config file `sudo nano /etc/nginx/nginx.conf`
```
server {
	listen 80;
        server_name cng-nmo-dev6.orc.gmu.edu www.cng-nmo-dev6.orc.gmu.edu;

        access_log /var/log/nginx/ner.access.log;
        error_log /var/log/nginx/ner.error.log;

        location = /favicon.ico { access_log off; log_not_found off; }
        location /media/ {
        autoindex on;
        alias /home/kbijari/project/neuroner-api/excel_forms/;
        }
	location / {
                proxy_set_header Host $http_host;
                proxy_set_header X-Real-IP $remote_addr;
                proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
                proxy_set_header X-Forwarded-Proto $scheme;
                proxy_pass http://unix:/home/kbijari/project/neuroner-api/ner.sock; # please note that the location of the sock file might be different depending on where you put your service
        }

    }
```
Test the configuration and run Nginx
```
$ sudo nginx -t
$ sudo systemctl restart nginx
```

## Dockerization
For ease of use, Dockerized version is added to this project. Follow the following steps to run a dockerized instance on your machine.

Downlaod/Clone the project and navigate the command line to the main folder and use the following command to run the dockerfile.
```sh
$ docker compose up --build
```
Please be patient as this process might take a while since it will install all neccessary packages and run the required scripts.

The dockerfile could be utilized to run the application on a virtual server in any desired way.


## API Usage
Once the API is running use the following commands to interact with the API:

### check the status of the API `localhost/status`
```json
{"api_status": "O.K.", "db_status": "213 structured article(s) are available in the database!", "ML_status": "O.K.", "Running_mode": "Production"}
```
### List of the latest processed articles `localhost/articles`
```json
{"message": "Latest 25 added articles!", "data": [{"pmid": "33593856", "fulltext": "{'abstract': ['Inhibitory interneurons expressing parvalbumin (PV) are central to cortical network dynamics, generation of \u03b3 oscillations, and cogniti...", "date": "2021-12-16 12:02:08.209000", "metadata": "{'SPE': {'mouse': 0.8132085392196685, 'rat': 0.5858033217082912, 'amplification': 0.41609866447770205, 'to': 0.41609866447774624, 'rattling': 0.515950..."}, {"pmid": "19812339", "fulltext": "{'abstract': ['Unlike the neocortex, sensory input to the piriform cortex is anatomically segregated in layer 1, making it ideal for studying the dend..."}, {"pmid": "34215735", "fulltext": "{'abstract': ['A deeper understanding of early disease mechanisms occurring in Parkinson\u2019s disease (PD) is needed to reveal restorative targets. Here ..."}, {"pmid": "33724184", "fulltext": "{'abstract': ['Despite the rising prevalence of methadone treatment in pregnant women with opioid use disorder, the effects of methadone on neurobehav...", "date": "2022-01-13 14:30:14.791000", "metadata": "{'SPE': {'mouse': 0.8131727763153118, 'human': 0.5964905560144959, 'rat': 0.5858033217083072, 'rodent': 0.4829841681304218, 'woman': 0.441107552497514..."}, {"pmid": "12332121112", "fulltext": "['Pending']", "date": "2021-12-10 12:24:36.607000"}, {"pmid": "123", "fulltext": "['fulltext is not available online!']", "date": "2021-12-10 12:36:32.227000"}, {"pmid": "34728755", "fulltext": "{'abstract': ['Adolescence represents a crucial period for maturation of brain structures involved in cognition. Early in life unhealthy dietary patte...", "date": "2021-12-10 12:38:17.045000"}, {"pmid": "33606195", "fulltext": "['fulltext is not available online!']", "date": "2021-12-10 12:48:56.181000"}, {"pmid": "34644562", "fulltext": "{'summary': ['Motion/direction-sensitive and location-sensitive neurons are the two major functional types in mouse visual thalamus that project to th...", "date": "2021-12-13 12:52:49.030000", "metadata": "{'SPE': {'mouse': 0.7604183032273315, 'cat': 0.0011533472761159347, 'monkey': 0.03833716085404135, 'mammalian': 0, 'human': 0.4598627764247569, 'non-h..."}, {"pmid": "23535779", "fulltext": "{'abstract': ['Chronic stress leads to heightened affective behaviors, and can precipitate the emergence of depression and anxiety. These disorders ar...", "date": "2021-12-13 11:55:29.449000", "metadata": "{'SPE': {'human': 0.48487166444498886, 'rat': 0.748117877297394, 'rodent': 0.41609866447771926}, 'STR': {'Sprague-Dawley': 0.5135505510681262}, 'GEN':..."}, {"pmid": "32144984", "fulltext": "{'abstract': ['The study of late-onset (sporadic) Alzheimer\u2019s disease (LOAD) has lacked animal models where impairments develop with aging. Oxidative ...", "date": "2021-12-13 12:03:01.250000", "metadata": "{'SPE': {'mouse': 0.8131727763152294, 'human': 0.459862776424836, 'mice\u00d7': 0.4160986644775985}, 'STR': {'wild type': 0.6539282211306325, 'Wild type AB..."}, {"pmid": "33230322", "fulltext": "{'abstract': ['In the vertebrate retina, the location of a neuron\u2019s receptive field (RF) in visual space closely corresponds to the physical location ...", "date": "2021-12-16 12:16:19.911000", "metadata": "{'SPE': {'mouse': 0.7663702738248613, 'eyewire': 0.4160986644777212, 'guinea pig': 0.483162461263244, 'mammalian': 0.41609866447775845, 'cricket': 0.4..."}, {"pmid": "33724148", "fulltext": "['fulltext is not available online!']", "date": "2021-12-16 09:07:25.586000"}, {"pmid": "33526823", "fulltext": "{'abstract': ['Schizophrenia (Sz) is a highly polygenic disorder, with common, rare, and structural variants each contributing only a small fraction o...", "date": "2022-01-13 10:48:41.854000", "metadata": "{'SPE': {'mouse': 0.8120602164119596, 'human': 0.5267482800776946, 'monkey': 0.4544358253316573, 'cat': 0.44226089977388355, 'rabbit': 0.4174360529047..."}, {"pmid": "34848542", "fulltext": "{'significance': ['Dendrites are long branching processes on neurons that contain small processes called spines that are the site of connections with ...", "date": "2022-01-13 10:45:31.825000", "metadata": "{'SPE': {'mouse': 0.8124064815620318, 'rat': 0.5858033217082136, 'human': 0.5597142588766171, 'rodent': 0.4632967492740205, 'rabbit': 0.41743605290485..."}, {"pmid": "32610135", "fulltext": "{'summary': ['The astrocytic response to injury is characterized on the cellular level, but our understanding of the molecular mechanisms controlling ...", "date": "2021-12-21 11:24:19.039000", "metadata": "{'SPE': {'mouse': 0.8130752136081246, 'human': 0.4598627764248918}, 'STR': {'C57BL/6': 0.4888347917105747, 'C57BL/6J': 0.4634187814879625, 'C57B6': 0...."}, {"pmid": "2017", "fulltext": "['fulltext is not available online!']", "date": "2021-12-22 12:36:06.060000"}, {"pmid": "34591324", "fulltext": "['fulltext is not available online!']", "date": "2021-12-28 10:06:50.310000"}, {"pmid": "34400780", "fulltext": "{}", "date": "2021-12-28 10:09:28.666000", "metadata": "{'SPE': {}, 'STR': {}, 'GEN': {}, 'DEV': {}, 'REG': {}, 'CEL': {}, 'EXP': {}, 'STA': {}, 'SLI': {}, 'PRO': {}, 'OBJ': {}, 'REC': {}, 'AGE': {}, 'OTH':..."}, {"pmid": "34758304", "fulltext": "{'summary': ['Cortical function relies on the balanced activation of excitatory and inhibitory neurons. However, little is known about the organizatio...", "date": "2021-12-28 11:13:42.881000", "metadata": "{'SPE': {'mouse': 0.691246913080224}, 'STR': {'Green': 0.41933276691215793, 'wt+1\u2013wt': 0.4160986644777001, 'Pvalb-IRES-Cre': 0.0004421362275149332, 'V..."}, {"pmid": "33614864", "fulltext": "{'abstract': ['Prenatal exposure to stress or glucocorticoids (GC) is associated with the appearance of psychiatric diseases later in life. Microglia,...", "date": "2022-01-14 10:06:03.109000", "metadata": "{'SPE': {'rat': 0.7660590653066157, 'rabbit': 0.44244494092482434, 'iudexamethasone': 0.44110755249755124, 'mouse': 0.17529676615077328, 'human': 0.04..."}, {"pmid": "33225275", "fulltext": "{'abstract': ['Donnai-Barrow syndrome, a genetic disorder associated to LRP2 (low-density lipoprotein receptor 2/megalin) mutations, is characterized ...", "date": "2022-01-10 12:24:27.817000", "metadata": "{'SPE': {'mouse': 0.8132075510110383, 'rabbit': 0.5172875353565906, 'human': 0.45986277642493756, 'sheep': 0.4174097696476369, 'rodent': 0.41609866447..."}, {"pmid": "30808398", "fulltext": "{'abstract': ['The fragile X premutation is a CGG trinucleotide repeat expansion between 55 and 200 repeats in the 5\u2032-untranslated region of the fragi...", "date": "2022-01-12 09:27:53.293000", "metadata": "{'SPE': {'mouse': 0.8132085389296803, 'rabbit': 0.5310384118049293, 'human': 0.5267482800775685, 'fragile x': 0.4160986644778506, 'fluorescent': 0.416..."}, {"pmid": "26537920", "fulltext": "{'abstract': ['Does repeat-associated non-AUG (RAN) translation play a role in fragile X-associated primary ovarian insufficiency (FXPOI), leading to ...", "date": "2022-01-12 17:28:27.983000", "metadata": "{'SPE': {'mouse': 0.813186376575071, 'human': 0.5856656339282734, 'woman': 0.4632967492740481, 'rabbit': 0.41743605290458274, 'yeast': 0.4160986644775..."}, {"pmid": "34134979", "fulltext": "{'abstract': ['In cortical microcircuits, it is generally assumed that fast-spiking parvalbumin interneurons mediate dense and nonselective inhibition...", "date": "2022-01-13 15:02:10.622000", "metadata": "{'SPE': {'rat': 0.6526888253611604, 'drosophila melanogaster': 0.562136708199426, 'rabbit': 0.4424449409249326, 'animal': 0.4160986644777091, 'drosoph..."}]}
```
### Request/Fetch an article `localhost/articles/pmid/<pmid>` (showing for pmid 30808398)
```json
{"state":"Ready","message":"Found!","data":{"pmid":"30808398","fulltext":{"abstract":["The fragile X premutation is a CGG trinucleotide repeat expansion between 55 and 200 repeats in the 5\u2032-untranslated region of the fragile X mental retardation 1 (FMR1) gene. Human carriers of the premutation allele are at risk of developing the late-onset neurodegenerative disorder, fragile X-associated tremor/ataxia syndrome (FXTAS). Characteristic neuropathology associated with FXTAS includes intranuclear inclusions in neurons and astroglia. Previous studies recapitulated these histopathological features in neurons in a knock-in mouse model, but without significant astroglial pathology. To determine the role of astroglia in FXTAS, we generated a transgenic mouse line (Gfa2-CGG99-eGFP) that selectively expresses a 99-CGG repeat expansion linked to an enhanced green fluorescent protein (eGFP) reporter in astroglia throughout the brain, including cerebellar Bergmann glia. Behaviorally these mice displayed impaired motor performance on the ladder-rung test, but paradoxically better performance on the rotarod. Immunocytochemical analysis revealed that CGG99-eGFP co-localized with GFAP and S-100\u00df, but not with NeuN, Iba1, or MBP, indicating that CGG99-eGFP expression is specific to astroglia. Ubiquitin-positive intranuclear inclusions were found in eGFP-expressing glia throughout the brain. In addition, intracytoplasmic ubiquitin-positive inclusions were found outside the nucleus in distal astrocyte processes. Intriguingly, intranuclear inclusions, in the absence of eGFP mRNA and eGFP fluorescence, were present in neurons of the hypothalamus and neocortex. Furthermore, intranuclear inclusions in both neurons and astrocytes displayed immunofluorescent labeling for the polyglycine peptide FMRpolyG, implicating FMRpolyG in the pathology found in Gfa2-CGG99 mice. Considered together, these results show that Gfa2-CGG99 expression in mice is sufficient to induce key features of FXTAS pathology, including formation of intranuclear inclusions, translation of FMRpolyG, and deficits in motor function.","The online version of this article (10.1186/s40478-019-0677-7) contains supplementary material, which is available to authorized users."],"introduction":["The fragile X premutation is defined as an expanded (CGG)n trinucleotide repeat in the 5\u2032-untranslated region of the FMR1 gene. Clinical and genetic studies of patients have indicated that carriers of the premutation allele, defined as a repeat length between 55 and 200 CGGs, are at risk of developing the late-onset neurodegenerative disorder, fragile X-associated tremor/ataxia syndrome (FXTAS) [27, 28, 35]. Elevated FMR1 mRNA levels found in cells of premutation carriers support the concept of a \u201ctoxic\u201d mRNA gain-of-function mechanism of pathophysiology in FXTAS [25], likely via sequestration of RNA-binding proteins by expanded CGG repeat-containing RNA [48]. Repeat-associated non-ATG translation (RAN) of a toxic in polyglycine-containing peptide, FMRpolyG, from the expanded-repeat mRNA may also contribute to FXTAS pathology [36, 47]. The principal clinical symptoms of FXTAS include progressive intention tremor and ataxia, peripheral neuropathy, neuropsychological involvement (anxiety, depression), and cognitive impairments and dementia at late stages of the disorder [4, 25, 26]. Radiologic changes observed by MRI include increased T2 signal (hyperintensities) in cerebral white matter and in the middle cerebellar peduncle (the \u201cMCP sign\u201d), as well as global brain atrophy [8]. Levels of FMR1 mRNA are elevated and levels of FMRP are slightly decreased in FXTAS. The neuropathological hallmark of FXTAS is the presence of spherical eosinophilic intranuclear inclusions in neurons and astroglia throughout the brain that are immunoreactive for ubiquitin [23, 24, 51].","The CGG KI mouse model of FXTAS shows similar neurobehavioral features that appear to be similar to those in FXTAS [20]. These include gait ataxia and visuomotor deficits in the ladder-rung [31] and rotarod tests [54], anxiety in the open field [10] and cognitive impairment [30, 32]. They also show ubiquitin-positive spherical inclusions in neurons and astrocytes similar to those found in FXTAS brains [6, 56]. The inclusions are found throughout the brain in all neocortical regions, hippocampus, hypothalamus, brain stem nuclei (e.g., reticular formation, inferior olivary and dentate nuclei) and in Bergmann glia in cerebellum [56, 59]. The topographical distribution and frequency of intranuclear inclusions increase with age and length of the CGG repeat segment, and also vary between brain regions [56, 59]. Pathology in the CGG KI mouse model differs from FXTAS pathology by the absence of tremors and the relatively few numbers of astrocytes with intranuclear inclusions [2]. Ubiquitin-positive inclusions were never observed in neurons or astroglia of WT mice in any brain region at any age [46].","To determine if expression of a CGG trinucleotide repeat expansion in astroglia is sufficient to induce pathology in astroglia, and to characterize the role of astroglia in FXTAS, we created a transgenic mouse line (Gfa2-CGG99-eGFP) that expresses a 99 CGG repeat expansion in astrocytes throughout the brain and in Bergmann glia in the cerebellum. Expression is driven by an astroglia-specific Gfa2 promoter fused to an eGFP reporter gene. In these mice, immunocytochemical analysis of eGFP expression patterns revealed that CGG99-eGFP expression co-localized with astroglia markers, but not with neuronal, microglia, or oligodendroglia markers, indicating that CGG99-eGFP expression was specific for astroglia and Bergmann glia. Double-immunostaining for ubiquitin revealed the presence of intranuclear inclusions in eGFP-positive glia throughout the brain, as well as ubiquitin-positive inclusions in the cytoplasm of astrocyte processes. Surprisingly, we also observed intranuclear inclusions in NeuN-positive neurons of the hypothalamus and neocortex, though these cells did not express the CGG99-eGFP transcript. The presence of cytoplasmic inclusions in astrocytes, ectopic inclusions and inclusions in neurons suggests a spread of pathology from astrocytes to neurons by as yet unknown mechanisms. Both glial and neuronal inclusions stained positive for the RAN translation product FMRpolyG [9, 52]. These results indicate that an expanded CGG-99 repeat in astroglia is sufficient to induce formation of ubiquitin- and FMRpolyG-positive intranuclear inclusions - key features of FXTAS pathology, and that the Gfa2-CGG99-eGFP mouse will be a valuable model to delineate neuron-astroglia interactions that contribute to FXTAS disease pathogenesis."],"conclusions":["Transgenic mice with high expression levels of an expanded CGG-99 trinucleotide repeat driven by a human Gfa2 promoter were developed to examine pathology in glia associated with the Fragile X premutation. Expression in glia was widespread throughout the brain, as visualized by the eGFP reporter expression within the Gfa2-CGG99-eGFP construct. eGFP fluorescence was limited to astroglia and Bergmann glia only, and a subset of these glia also developed ubiquitin-positive intranuclear inclusions between the ages of 4\u201316\u2009months. Expression of eGFP was not observed in microglia immunolabeled with Iba1 or oligodendroglia immunolabeled with MBP, and intranuclear inclusions were never observed in these glial subtypes. Although we do not yet have direct evidence for cell-to-cell spread of pathology, the unexpected finding of intranuclear inclusions in NeuN-labelled neurons, particularly in the hypothalamus, opens the possibility for this type of transfer of pathology in our Gfa2-CGG99 mouse model of FXTAS. The presence of the RAN translation product FMRpolyG in the astrocyte inclusions indicates that this mechanism of pathology in trinucleotide repeat expansion disease may not be limited to neurons, and may occur in astrocytes in FXTAS patients, though this is yet to be documented. Taken together, our results highlight that FXTAS pathology is complex involving both astrocytes and neurons and their possible interactions. Our findings thus provide important new insights that should be considered when developing therapies for FXTAS in human patients."],"additional files":["","Figure S1. Expression vector maps used to generate the (A) EGFP-CGG99-EGFP or (B) EGFP-CGG11-EGFP transgenic mouse lines. Figure S2. Statistical results for behavioral experiments. (DOCX 339 kb)",""],"acknowledgements":["We are grateful to s. Grete Adamson for excellent support in electron microscopy (Department of Medical Pathology and Laboratory Medicine, UC Davis). We thank Emily Doisy for her careful reading of drafts of this manuscript and many helpful suggestions. Supported by grants from the NIH (NINDS NS079775 and NINDS RL1 to R.F.B; NS062411; NIA RL1 AG032119 to P.J.H; AG033082 to A.R.L.S.), Roadmap Consortium NIDCR UL1 DE 19583, and NICHD U54D079125 to the UC Davis M.I.ND. Institute IDDRC.","The datasets used and/or analysed during the current study are available from the corresponding author on reasonable request."],"authors\u2019 contributions":["RFB, ARLS, PJH, RKH and RW conceived of the project and contributed to writing the manuscript. HJW carried out the majority of the histology, immunohistochemistry, EM studies and contributed to the interpretation of the histological results. KDM performed the LCM and single-cell PCR and helped write the manuscript. MRH performed behavioral tests, carried out histology, immunohistochemistry and helped write the manuscript. JJS performed behavioral tests. KK performed the statistical analyses. ARLS designed the transgene constructs and BLS generated the transgenic mice and assisted with their initial characterization. CR assisted with making of the CGG construct. LAS and SNH carried out the FMRpolyG immunohistochemistry. All authors read and approved the final manuscript.All authors read and approved the final manuscript."],"publisher\u2019s note":["The authors declare that they have no competing interests.","Springer Nature remains neutral with regard to jurisdictional claims in published maps and institutional affiliations."],"references":[]},"date":{"$date":"2022-01-12T09:27:53.293Z"},"metadata":{"SPE":{"mouse":0.8132085389296803,"rabbit":0.5310384118049293,"human":0.5267482800775685,"fragile x":0.4160986644778506,"fluorescent":0.4160986644777542,"protein":0.4160986644777542,"gfa":0.4160986644776604,"murine":0},"STR":{"wild type":0.652244585351615,"gfa2-cgg99":0.6374147295811564,"Wild":0.6362312253131678,"Wild type AB":0.6359582762538778,"Green":0.6350166839239066,"CGG":0.6342242456591831,"transgenic":0.6233282813944527,"C57BL/6J":0.5303042851405713,"gfa2-cgg11":0.5297010233777182,"C57BL/6":0.48883479171042615,"gfa2-cgg11 transgenic":0.4829841681303583,"C57BL":0.4633342488051481,"gfa2":0.4632967492739677,"gfa2-cgg99 transgenic":0.4411075524974691,"C57BL/6JRj":0.42470025265819056,"C57BL/6N":0.42102234388454185,"C57BL/6J + FVB":0.4175122415478776,"C57BL6/J x CBA":0.4166830170404985,"C57B6/SJL":0.41663811537657724,"C57BL/J":0.41643595783420806,"Brown":0.4161286645526932,"C57BL/6H":0.4161211648713603,"gfap":0.4160986644777542,"gfap-cgg99":0.4160986644777542,"transgenic gfa2-cgg99-":0.4160986644776954,"c3h/hej":0.41609866447761856,"non-transgenic":0.41609866447755406,"cgg128":0,"cgg159":0},"GEN":{"male":0.4411075524975342,"adult":0.4160986644775795},"DEV":{"adult":0.6375603149163269,"old":0.45449365127696956,"oocyte":0.41609866447761856},"REG":{"neocortex":0.7298848459922049,"intranuclear":0.6366175271889996,"hypothalamus":0.6272240118116157,"cerebellum":0.5681027691348038,"neocortex-->>frontal-->>motor":0.5618475081848338,"spinal cord-->>ventral":0.5548102897458024,"hippocampus-->>dentate gyrus-->>ventral":0.5548102897458024,"hippocampus":0.539084594595898,"amygdala":0.5364095511337245,"basal ganglia-->>nucleus accumbens-->>core":0.529953690397321,"neocortex-->>retrosplenial area-->>granular":0.529734727077252,"pallium-->>cortex":0.5201253271848783,"brainstem":0.5194781872488616,"cerebellum-->>cerebellar cortex":0.5025691468818242,"cerebellum-->>vermis-->>posterior":0.5009569571136764,"hypothalamus-->>posterior hypothalamic nucleus":0.48301506336195893,"hippocampus-->>ca1":0.4780848480272284,"peripheral nervous system-->>abdominal-->>segment 2-3":0.4648033423662199,"peripheral nervous system-->>abdominal-->>segment 2":0.46402597969803394,"peripheral nervous system-->>abdominal-->>segment 2-4":0.4633529195821493,"myelencephalon-->>right-->>stripe 2":0.4633192189118973,"myelencephalon-->>left-->>stripe 2":0.4633192189118973,"peripheral nervous system-->>abdominal-->>segment 2-5":0.463302366872824,"spinal cord-->>s2":0.4632995580891962,"hippocampus-->>dentate gyrus-->>granule layer":0.4530642893239477,"neocortex-->>parietal":0.4436032975281148,"brainstem-->>hindbrain-->>reticular formation":0.4428931791992859,"peripheral nervous system-->>abdominal-->>segment 1":0.44198237355461745,"myelencephalon-->>right-->>stripe 1":0.4416042215635603,"myelencephalon-->>left-->>stripe 1":0.4416042215635603,"peripheral nervous system-->>cornea-->>central":0.4414219437161898,"peripheral nervous system-->>cornea-->>superior":0.44130970655041835,"peripheral nervous system-->>abdominal-->>segment 1-2":0.44130689997493744,"spinal cord-->>lumbar-->>white matter":0.4412311105168868,"spinal cord-->>s1":0.44112721354116147,"pallium-->>cortex-->>proximal":0.44112440491544536,"neocortex-->>frontal-->>layer 3":0.4227279090220837,"neocortex-->>frontal-->>primary motor":0.42134588777131354,"hippocampus-->>dentate gyrus-->>molecular layer":0.42053608729412284,"hypothalamus-->>paraventricular hypothalamic nucleus":0.42000026238415455,"basal ganglia-->>substantia nigra":0.41848871691001444,"hippocampus-->>ca1-->>stratum radiatum":0.41813236755684546,"neocortex-->>somatosensory-->>layer 1":0.41693149696567766,"hippocampus-->>dentate gyrus-->>hilus":0.4169035006549008,"myelencephalon-->>inferior olive":0.4165757100170321,"hippocampus-->>ca1-->>stratum oriens":0.4164747646403238,"myelencephalon-->>solitary nucleus":0.4164663506825005,"basal ganglia-->>striatum-->>rostral":0.41625871658745783,"main olfactory bulb-->>stratum granulosum-->>outer":0.41622783731933666,"brainstem-->>cochlear nucleus":0.4161436017339185,"Olfactory Bulb":0.4161239426783476,"thalamus-->>paracentral nucleus":0.4161211341156157,"hypothalamus-->>posterior hypothalamic nucleus-->>mammilary body":0.41612113411556734,"peripheral nervous system-->>tail":0.4161183255213451,"neocortex-->>temporal-->>inferior":0.4161014732929662,"thalamus-->>parafascicular nucleus":0.4161014732929146,"maze":0.41609866447779775,"suprachiasmatic nucleus":0.41609866447778066,"rim region":0.41609866447775556,"cerebellar lobuli":0.4160986644777522,"ca1 stratum radiatum":0.4160986644777507,"neocortical layers":0.41609866447772764,"zone":0.4160986644777128,"rostral neocortex":0.41609866447771116,"periventricular nucleus":0.41609866447770255,"subcortical brain":0.4160986644776973,"paraventricular":0.4160986644776708,"peripheral region":0.41609866447764854,"rim":0.41609866447764854,"pars granulosa":0.41609866447760235,"hippocampus-->>ca1-->>dorsal":0.006300901341004056,"basal ganglia-->>striatum":0.005285718540041756,"Central nervous system":0.00374117591829215,"peripheral nervous system-->>dorsal root ganglion":0.0024595921143553134,"anterior olfactory nucleus-->>pars principalis-->>middle":0.0002695024050232697,"cerebellum-->>deep cerebellar nuclei":1.4043760353577017e-05,"cerebral white matter":0,"dentate nucleus":0,"nuclear lamina":0,"spinocerebellar":0,"spinocerebellar ataxia":0},"CEL":{"glia":0.6728046966879605,"principal cell-->>pyramidal":0.6524330611254694,"glia-->>astrocyte":0.6471843793784843,"astroglia":0.6358233042638591,"glia-->>microglia":0.5905789806331877,"glia-->>astrocyte-->>protoplasmic":0.5299578881370419,"glia-->>radial":0.48298981634803784,"oligodendroglia":0.4829841681305055,"principal cell-->>pyramidal-->>developing":0.46384966851958237,"interneuron-->>bipolar-->>rod":0.4636974557863599,"principal cell-->>granule":0.4631119936867222,"principal cell-->>purkinje":0.4444402189757172,"principal cell-->>pyramidal-->>typical":0.44217561381485376,"principal cell-->>motoneuron-->>acc":0.44113014460293526,"principal cell-->>induced pluripotent stem cell-(ipsc)-derived":0.42301631921797,"glia-->>astrocyte-->>gfap-positive":0.4218243906386976,"sensory-->>olfactory":0.41660649333382416,"glia-->>oligodendrocyte":0.41655857586427636,"principal cell-->>pyramidal-->>complex":0.4165501188849222,"interneuron-->>propriospinal-->>descending":0.4164006649828909,"interneuron-->>descending":0.4164006649828909,"sensory-->>somatic":0.41635270798194446,"interneuron-->>periventricular":0.4162511215732423,"principal cell-->>ganglion-->>ret-positive":0.41614667026090435,"principal cell-->>motoneuron-->>arm":0.41611278478266056,"principal cell-->>pyramidal-->>giant cell":0.4161127847825802,"principal cell-->>ganglion-->>medium":0.41610148860250684,"principal neuron":0.4160986644778101,"neuron-astroglia":0.41609866447778543,"fibrou":0.41609866447774024,"non-neuronal":0.41609866447772564,"neuronal/glial":0.416098664477659,"bergman glia":0.41609866447758537,"glia-->>microglia-->>Iba1-positive":0.4160986644775623,"principal cell-->>ganglion":0.011031642302715056,"protocerebrum-->>mushroom body-->>alpha":0.000324565319025083,"principal cell-->>spinocerebellar":7.906343645444625e-05,"principal cell-->>t2":5.92999217848833e-05,"interneuron-->>interneuron-specific interneuron":0,"dorsal root ganglion":0,"glia-->>microglia-->>type 2":0,"interneuron-->>basket-->>type 2":0},"EXP":{"Control":0.8473650103746375,"Knock-in":0.6151944772009796,"fragile x-associated tremor/ataxia syndrome":0.5784132200667296,"cgg knock-in":0.5784132200666541,"gfa2-cgg99":0.5297010233778262,"fragile x":0.500451878437257,"Ethanol":0.48348101135509125,"ubiquitin":0.4829841681304954,"Learning":0.46354860670133663,"ataxia":0.46329674927406855,"syndrome":0.46329674927406855,"ubiquitin-positive":0.4632967492740466,"cgg159 knock-in":0.4632967492739658,"treatment":0.4411141835774247,"fragile x-associated tremor/ataxia":0.441107552497745,"tremor":0.44110755249769085,"associated":0.44110755249769085,"gfa2-cgg11":0.4411075524976902,"fragile x-associated tremor":0.44110755249767253,"cgg168 knock-in":0.4411075524976121,"Knockout":0.4193858151762042,"Transgenic":0.41740159742540367,"DMSO":0.41714419660299745,"fragile":0.4160986644778506,"retardation":0.4160986644778506,"tremor/ataxia syndrome":0.41609866447778543,"pathogenesi":0.41609866447778543,"in":0.4160986644777704,"immuno":0.4160986644777669,"neun-positive":0.4160986644777589,"ubiquitin-":0.416098664477743,"gfa2":0.41609866447772886,"fragile x premutation":0.4160986644777248,"conditioned stimulu":0.4160986644777218,"unconditioned stimulu":0.4160986644777218,"negative control":0.4160986644777213,"cued fear conditioning":0.4160986644777154,"cgg128 knock-in":0.41609866447771515,"gfap-positive":0.41609866447771515,"ectopic":0.41609866447770827,"freezing":0.4160986644777076,"fragile x mental":0.41609866447770605,"vehicle":0.41609866447770316,"mbp-immunopositive":0.41609866447769206,"c9orf72-al":0.41609866447768373,"positive control":0.4160986644776812,"neurodegenerative disorder":0.41609866447767996,"dapi-positive":0.4160986644776794,"gfap positive":0.4160986644776737,"gfap negative":0.4160986644776737,"rotarod":0.4160986644776713,"better motor learning":0.4160986644776713,"motor":0.4160986644776604,"shapiro-wilk test":0.41609866447764726,"kolmogorovo-smirnov test":0.41609866447764726,"rinse":0.4160986644776373,"pb":0.4160986644776373,"control":0.4160986644776151,"mrh":0.4160986644776151,"water control":0.4160986644775832,"anesthetized":0.4160986644775735,"sodium pentobarbital":0.4160986644775735,"walking":0.41609866447756527,"microwave treatment":0.4160986644775555,"wildtype":0.41609866447755406,"calsyntenin-1 knockout":0.416098664477513,"impaired motor":0.4160986644775089,"Dbx1-expression":0.00023198317450712214,"FGF10 Overexpression":0.00017235092618658743,"FGF9 Overexpression":0.00016572424203836755,"conditional Knock-Out":0.0001325881846097332,"zDHHC15 overexpression":6.630288444983701e-05,"conditional knockout IL-33":2.6523264193251128e-05,"78Q Overexpression":6.631079870977885e-06,"toxic":0,"fragile x-associated":0,"progressive intention tremor":0,"peripheral neuropathy":0,"neuropsychological involvement":0,"anxiety":0,"cognitive impairment":0,"global brain atrophy":0,"gait ataxia":0,"visuomotor deficit":0,"cggn knock-in":0,"neurobehavioral":0,"motor deficit":0,"fragile x-associated tremor/":0,"pten":0,"neuroligin-3 r451c knock-in":0,"neuroligin-3 mutation":0,"neurodegenerative disease":0,"neurodevelopmental disorder":0,"fragile x mental retardation":0,"alzheimer\u2019s pathology":0,"parkinson\u2019s disease":0,"polyglutamine disease":0,"neurodegenerative":0,"repeat":0,"c9orf72":0,"deletion of the n17 domain of the huntingtin gene":0},"STA":{"immunostaining":0.7768438844697014,"green fluorescent protein":0.6329463296563214,"fluorescent":0.6280213607341021,"fluorescent protein":0.6237514618313309,"dapi":0.5419015219810793,"fluorescent protein fluorescence":0.5004518784372495,"enhanced green fluorescent protein":0.4889090434834017,"protein":0.48298416813045036,"immunofluorescent":0.4829841681304467,"red fluorescent protein":0.4698693438225199,"ubiquitin-stained":0.4632967492740565,"alexa 568":0.46329674927382813,"Alexa Fluor 488":0.44376975005987196,"iba1":0.44110755249771805,"cresyl violet":0.4411075524976491,"neun":0.44110755249763367,"uranyl acetate":0.4229231187391478,"yellow fluorescent protein":0.41789015125672113,"dextran coupled texas red fluorophore":0.4161190947391296,"neutral":0.41609866447783794,"neun-labelled":0.41609866447783694,"neuropathology":0.4160986644778057,"dab immuno-peroxidase":0.4160986644777914,"dapi-stained":0.4160986644777762,"positive":0.4160986644777669,"yellowish-green fluorescence":0.41609866447776234,"h&e":0.41609866447775823,"immunocytochemistry":0.41609866447775823,"ubiquitin staining":0.41609866447775823,"protein fluorescence":0.4160986644777522,"immunofluorescent staining":0.4160986644777499,"ubiquitin":0.41609866447774413,"ubiquitin-immunolabeled":0.41609866447773436,"fluorescence":0.41609866447772864,"red immunofluorescent":0.41609866447769855,"green immunofluorescence":0.4160986644776711,"neutral red":0.4160986644776691,"green":0.4160986644776628,"multiplex immunofluorescent":0.416098664477659,"borohydride":0.416098664477659,"alexa 468":0.41609866447764493,"gfap":0.4160986644776349,"double immunofluorescent staining":0.4160986644776272,"red/yellow fluorescent":0.41609866447761257,"haematoxylin":0.4160986644775979,"entellan":0.4160986644775979,"dapi staining":0.41609866447758065,"double":0.4160986644775785,"lead aspartate":0.4160986644775361,"alexa 488 fluorescent":0.4160986644775301,"h&e-stained":0.41609866447752786,"hoechst":0.41609866447752486,"alexa":0.41609866447747046,"488":0.41609866447747046,"osmium tetroxide/uranyl acetate":0.05233275487252864,"Osmium tetraoxide":0.004501493665517692,"nissl":0},"SLI":{"coronal":0.6454036472502287,"horizontal":0.4747711928922682,"Sagittal":0.42613115114539946},"PRO":{"in vitro":0.8446131419693651,"whole mount":0.4411075524975987,"in vivo":0.17743577890231316},"OBJ":{"water":0.6522903920307306,"electron microscopy":0.6402754395123162,"dry":0.565357567383646,"electron":0.46329674927402054,"microscopy":0.4160986644776651},"REC":{},"AGE":{"month":0.4829841681304273,"8 months of age":0.4411075524976257,"day":0.44110755249762185,"4 months of age":0.41609866447774824,"10 month":0.41609866447774824,"4\u201316 month":0.41609866447774413,"6 months of age":0.4160986644777174,"7 and":0.4160986644777174,"4\u20138 month old":0.41609866447771715,"16 month old":0.4160986644776879,"24 weeks of age":0.41609866447767396,"7 months of age":0.41609866447766386,"old":0.41609866447766386,"4 and 16 months old":0.41609866447763516,"16 months of age":0,"3 months of age":0},"OTH":{}}}}
```

<!-- issues and errors one might face -->
## Speed-bumps

### Should I change any passwords/addresses/names?
There is a file in the parent directory named `/base/parameters.py`. This file contains all passwords/url one needs to change in case of transitions. Please make sure to follow along with the help and formats provided there.


### Services are not running and throw errors
Make sure your path/naming fashion is consistant. Also, make sure the users/groups have the right privileges.

### Code not running and only shows the start page
Make sure user permissions, addresses, and groups are correctly set

### Some of the used packages do not exist anymore
Install the closest version of the none existing packages

### Operating system is updated and reboot is needed! What happens to the services?
Make sure you 'activate' and 'enable' the services. This will ensure that the services will auto-start on a reboot. In case you missed to do so, you need to reactivate the services.

### Other mulfunctions
Read and check the log files carefully to see which services are acting up.

<!-- CONTRIBUTING -->
## Contributing

Contributions are what make the open source community such an amazing place to be learn, inspire, and create. Any contributions you make are **greatly appreciated**.

1. Fork the Project
2. Create your Feature Branch (`git checkout -b feature/AmazingFeature`)
3. Commit your Changes (`git commit -m 'Add some AmazingFeature'`)
4. Push to the Branch (`git push origin feature/AmazingFeature`)
5. Open a Pull Request (if you like)


<!-- LICENSE -->
## License
Distributed under the GPL 3.0 License.

<!-- CONTACT -->
## Contact

- Kayvan Bijari - kbijari[@]gmu[dot]edu
- Giorgio Ascoli - ascoli[@]gmu[dot]edu (PI)
- NeuroMorpho.Org administration team - nmadmin[@]gmu[dot]edu
<!-- - Project Link: [Metadata Portal](http://cng-nmo-meta.orc.gmu.edu/) -->
<!-- - Source Link: [GitHub](https://github.com/NeuroMorpho/metadata-portal) -->


<!-- ACKNOWLEDGEMENTS -->
## Acknowledgements
* [NeuroMorpho.Org](http://neuromorpho.org/)
* [Office of Research Computing](https://orc.gmu.edu/)
* [George Mason University](https://www2.gmu.edu/)


<!-- MARKDOWN LINKS & IMAGES -->
[metadata-portal]: http://cng-nmo-meta.orc.gmu.edu/
[metadata-portal-paper]: https://braininformatics.springeropen.com/articles/10.1186/s40708-020-00103-3
[MongoDB]: https://www.mongodb.com/docs/manual/tutorial/install-mongodb-on-ubuntu/
