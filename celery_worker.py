from base import celery
from base.factory import create_app
from base.celery_utils import init_celery
app = create_app()
init_celery(celery, app)