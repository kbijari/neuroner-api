import json
import re
import pandas as pd

def flatten_json(d):
    skip = ['authorGroup', 'image', 'referenceList']
    out = {}
    content = []
    def flatten(x, name=''):
        if type(x) is dict:
            for k, v in x.items():
                if k in skip: continue
                flatten(v)
        elif type(x) is list:
            for a in x:
                flatten(a)
        else:
            if len(x):
                content.append(re.sub(r"[\n\t\s]+", " ", str(x)).strip())
    flatten(d)
    return content

with open('./fullText.json', 'r') as jfile:
    lines = jfile.read()
    # modify names
    lines = re.sub(r'ObjectId*.+?\"\)',
                          r'{"$oid": 1}',
                          lines)
    lines = re.sub(r'BinData*.+?\"\)',
                          r'{"bindata": 1}',
                          lines)
    lines = re.sub(r'new Date*.+?\"\)',
                          r'{"date": 1}',
                          lines)
    # convert to json
    # print(lines)
    data = json.loads(lines)


# print(data)
print(flatten_json(data))