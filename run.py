from base.factory import create_app
import base


app = create_app(celery=base.celery)