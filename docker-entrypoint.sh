#!/bin/sh

set -o errexit
#set -o pipefail
set -o nounset

mongo_ready() {
    python << END
import os
import sys
from pymongo import MongoClient
import urllib
import parameters

try:
    username = urllib.parse.quote_plus(os.environ.get('DB_USER', 'root'))
    password = urllib.parse.quote_plus(os.environ.get('DB_PASS', 'metadata@nmo2006'))
    db_host = os.environ.get('DB_HOST', 'cng-nmo-dev6.orc.gmu.edu')
    mongo_client = MongoClient('mongodb://%s:%s@%s:27017' % (username, password, db_host))
    print(username, password, db_host)
    print(mongo_client.server_info())
except Exception as e:
    print('Error calling Mongo!', str(e))
    sys.exit(-1)
END
}

redis_ready() {
    python << END
import sys

from redis import Redis
from redis import RedisError

try:
    redis = Redis.from_url("${CELERY_BROKER_URL}", db=0)
    redis.ping()
except  Exception as e:
    print('Error calling Redis!', str(e))
    sys.exit(-1)
END
}

until mongo_ready; do
  >&2 echo " ========================= Waiting for Mongo to become available... ========================= "
  sleep 5
done
>&2 echo " ========================= Mongo is available ========================= "

until redis_ready; do
  >&2 echo " ========================= Waiting for Redis to become available... ========================= "
  sleep 5
done
>&2 echo " ========================= Redis is available ========================= "

echo " ========================= Download necessary Corpora ========================= "

echo "downloading spacy packages"
python -m spacy download en_core_web_lg
python -m spacy download en_core_web_sm

echo "downloading nltk corpora"
python << END
import nltk
nltk.download('all')
END

echo "Write terms from file to the database"
python term-relation-extraction/relation-extraction.py

echo " ========================= Done! ========================= "

exec "$@"