### imports
import threading
import flask
from flask import json
from flask_restful import Resource, Api, reqparse
from flask import request, jsonify
from pymongo import MongoClient
import json
from bson import json_util
import time
import urllib.parse
from celery import Celery
# from bson.json_util import dumps, loads

### local imports
# from api_functions import web_text_extract
# from .task import web_text_extract
# from .celery import app




### control settings
development = 0 # run in production or development

### initialize the api
app = flask.Flask(__name__)
# app = factory.create_app(celery=app.celery)
if development:
    app.config["DEBUG"] = True
api = Api(app)

# celery configs
# app.config['CELERY_BROKER_URL'] = 'redis://localhost:6379/0'
# app.config['CELERY_RESULT_BACKEND'] = 'redis://localhost:6379/0'
# celery = Celery(app.name, broker=app.config['CELERY_BROKER_URL'])
# celery.conf.update(app.config)

from api_functions import web_text_extract

### setup database connection
username = urllib.parse.quote_plus('root')
password = urllib.parse.quote_plus('metadata@nmo2006')
mongo_client = MongoClient('mongodb://%s:%s@cng-nmo-dev6.orc.gmu.edu:27017' % (username, password))
db = mongo_client['structured_data'] # database
# test writing in the db
# db.articles.insert_one({'pmid':'124', 'text':'second test'})



### local functions
def extract_structured_data(pmid):
    # time.sleep(10000)
    fulltext = web_text_extract.delay(pmid)
    # insert to the db
    db.articles.insert_one({'pmid':pmid, 'fulltext':fulltext})


### class resources
# show status of the api/db/model...
class Status(Resource):
    def get(self):
        status = "O.K."
        try:
            article_count = db.articles.count_documents({})
            db_msg = "{} structured article(s) are available in the database!".format(article_count)
        except Exception as e:
            db_msg = "DB connection failed"

        return {'api_status':status, 'db_status':db_msg, 'ML_status':'None'}
# add endpoint
api.add_resource(Status, '/status', '/')  # return api status

# list of recent articles
class Articles(Resource):
    """ return a number of recent added articles
    """
    def get(self):
        try:
            max_retrive = 5
            data = []
            articles = db.articles.find({}, {'_id': False}).limit(max_retrive)
            for article in articles:
                tmp = {}
                for key, val in article.items():
                    if len(str(val))>50:
                        tmp[key] = str(val)[:50] + '...'
                    else:
                        tmp[key] = str(val)
                data.append(tmp)
                # data.append(json.loads(json_util.dumps(a)))
            return {'message':'Latest {} added articles!'.format(max_retrive), 'data':data}
        except Exception as e:
            message = "Error while retriving articles ({})!".format(str(e))
            return {'message': message}
# end point
api.add_resource(Articles, '/articles')  # '/ entry point for articles

# return or request articles by pmid
class ArticleByPMID(Resource):
    """ return an article by PMID
    """
    def get(self, pmid):
        try:
            metadata = []
            article = db.articles.find_one({'pmid':pmid}, {'_id': False})
            if article:
                return {'state': 'Ready', 'message':'Found!', 'metadata':json.loads(json_util.dumps(article))}
            else:
                # threading.Thread(target=extract_structured_data(pmid)).start()
                extract_structured_data(pmid)
                return {'state': 'Pending', 'message':'This PMID is not Available! Metadata extraction is requested.'}
        except Exception as e:
            message = "Error while retriving ({})!".format(str(e))
            return {'message': message}
# end point
api.add_resource(ArticleByPMID, '/articles/pmid/<pmid>', endpoint='article_by_pmid')


### run main and api
if __name__ == '__main__':
    app.run(threaded=True)  # run our Flask app