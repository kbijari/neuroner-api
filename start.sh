#!/bin/sh

cd /app

if [ $# -eq 0 ]; then
    echo "Usage: start.sh [PROCESS_TYPE](server/runserver/worker)"
    exit 1
fi

PROCESS_TYPE=$1

# run the service with gunicorn
if [ "$PROCESS_TYPE" = "server" ]; then
    if [ "$DJANGO_DEBUG" = "true" ]; then
        gunicorn \
            --reload \
            --bind 0.0.0.0:8000 \
            --workers 2 \
            --worker-class eventlet \
            --log-level DEBUG \
            --access-logfile "-" \
            --error-logfile "-" \
            dockerapp.wsgi
    else
        gunicorn \
            --bind 0.0.0.0:8000 \
            --workers 2 \
            --worker-class eventlet \
            --log-level DEBUG \
            --access-logfile "-" \
            --error-logfile "-" \
            dockerapp.wsgi
    fi
elif [ "$PROCESS_TYPE" = "worker" ]; then
    celery \
        -A celery_worker.celery worker --loglevel=INFO
elif [ "$PROCESS_TYPE" = "worker2" ]; then
    echo "Running Celery worker"
    celery \
        --app dockerapp.celery_app \
        worker \
        --loglevel INFO
# run python service
elif [ "$PROCESS_TYPE" = "runserver" ]; then
    echo "Running Flask API"
    python manage.py
fi
