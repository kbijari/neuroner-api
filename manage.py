from base import factory
import base

if __name__ == "__main__":
    app = factory.create_app(celery=base.celery)
    app.config['TESTING'] = True
    app.run(host="0.0.0.0", debug=True)


