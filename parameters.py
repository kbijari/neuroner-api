# list of parameters used in the code
# make sure that you don not change the variable_names, this might result in crash in the code
# example:
# variable_name = "value

# Address/url for the server this code (NEURO-NER) is running on (avoid using http/https in the address - use format as example)
LocalRunningServer = "cng-nmo-dev6.orc.gmu.edu"

# external link to send extracted metadata information to (provide a link for a recieving api to call - use format as example)
ExternalMetadataPortal = "http://cng-nmo-meta.orc.gmu.edu/api/"


