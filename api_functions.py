
### imports
from bs4 import BeautifulSoup
import requests
import time

# celery delay
# from .celery import app
from .api import app


# extract raw text from pubmed online
@app.task
def web_text_extract(pmid):
    time.sleep(100)
    return ['test']
    try:
        headers = {'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/39.0.2171.95 Safari/537.36'}
        # convert pmid to pmcid
        convert_api = "https://www.ncbi.nlm.nih.gov/pmc/utils/idconv/v1.0/?tool=my_tool&email=my_email@example.com&ids={}&versions=no&format=json"
        # link to full text from pumbed
        article_url = "https://www.ncbi.nlm.nih.gov/pmc/articles/{}/"
        # request for the full text
        req = requests.get(convert_api.format(pmid), headers=headers, timeout=1000).json()
        if 'pmcid' in req['records'][0]:
            # print('fetching ... ', req['records'][0]['pmcid'])
            pmcid = req['records'][0]['pmcid']
        else:
            return ["fulltext is not available online!"]
        
        # get full-text of the article using the link
        html_content = requests.get(article_url.format(pmcid), headers=headers, timeout=1000).text        
        # Parse the html content
        soup = BeautifulSoup(html_content, "lxml")
        article_data = {}
        # print(soup.title.text)
        for sec in soup.findAll("div", {"class": "tsec sec"}):
            # print('============================ new sec ===================================')
            for h in sec.find_all("h2"):
                # print(h.text)
                section_name = h.text
            article_data[section_name.lower()] = []
            for par in sec.find_all("p"):
                # print(par.text.strip())
                article_data[section_name.lower()].append(par.text.strip())
    except Exception as e:
        return ["Text extraction failed! ({})".format(str(e))]
    
    # return the extracted text
    return article_data