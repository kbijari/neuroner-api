# syntax=docker/dockerfile:1


# Based image
FROM python:3.8.10

# Label
LABEL maintainer="kbijari@gmu.edu"

# Set environment variables and python flags
ENV PIP_DISABLE_PIP_VERSION_CHECK 1
ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1

# Settings, updates, compilers, and OS libraries
#RUN apk update
RUN apt-get update
# Cleanup after updates
RUN rm -rf /var/lib/apt/lists/*

# Set work directory
WORKDIR /app

# Copy the project
COPY . .

# Install dependencies
#RUN pip3 install --upgrade pip3
RUN pip3 install -r requirements-docker.txt

# Set permisions
RUN chmod +x /app/start.sh
RUN chmod +x /app/docker-entrypoint.sh
# RUN chmod a+rwx /app/database-data/

# Run the services
ENTRYPOINT ["/app/docker-entrypoint.sh"]
CMD ["/app/start.sh", "runserver"]
