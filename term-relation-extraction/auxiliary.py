#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Jun 21 14:38:38 2021

@author: kayvan

Auxiliary Functions
"""

# =============================================================================
# %% imports
# =============================================================================
import re
import pandas as pd
from collections import Counter
from Levenshtein import distance, jaro_winkler, jaro, hamming

from string import digits
remove_digits = str.maketrans('', '', digits)

from Schwartz import extract_abbreviation_definition_pairs

# =============================================================================
# %% resolve abbreviations
# =============================================================================
# roman to int
def roman_to_int(s):
    if s == '':
        return s
    punc = ''
    if '.' in s:
        s = s.replace('.', '')
        punc = '.'
    elif ',' in s:
        s = s.replace(',', '')
        punc = ','
    elif ')' in s:
        s = s.replace(')', '')
        punc = ')'
    rom_val = {'I': 1, 'V': 5, 'X': 10, 'L': 50, 'C': 100, 'D': 500, 'M': 1000}
    int_val = 0
    for i in range(len(s)):
        if i > 0 and rom_val[s[i]] > rom_val[s[i - 1]]:
            int_val += rom_val[s[i]] - 2 * rom_val[s[i - 1]]
        else:
            int_val += rom_val[s[i]]
    if punc == '':
        return str(int_val)
    else:
        return str(int_val) + punc

# resolve roman mentions in text
def resolve_roman(string, sep=' ', look_deep=True):
    flag = False
    resolved = False
    if '-->>' in string:
        string = string.replace('-->>', ' -->> ')
        flag = True
#    pattern = "^M{0,4}(CM|CD|D?C{0,3})(XC|XL|L?X{0,3})(IX|IV|V?I{0,3})(\.|,){0,1}$" original
    pattern = "^(IX|IV|V?I{0,3})(\.|,|\)){0,1}$"
    word_list = []
    for word in string.split(sep):
        if look_deep != False:
            for char in ['/', '-']:
                if char in word:
                    word = resolve_roman(word, sep=char)
        if bool(re.match(pattern, word)) and not ('\n' in word) and len(word):
            word = roman_to_int(word)
            resolved = True
        word_list.append(word)
    if sep != ' ' and resolved:
        new_string = '-'.join(word_list)
    else:
        new_string =  sep.join(word_list)
    if flag:
        new_string = new_string.replace(' -->> ', '-->>')
    return new_string

# test
#print(resolve_roman('Shank3-/-:CaMKIIalpha-Cre'))
#print(resolve_roman('olfactory pit-->>ciliary band I-->>right', look_deep=False))
#print(resolve_roman('this is a \n test sentence with .\n'))
#print(resolve_roman('Shank3+/+:CaMKIIα-Cre hemizygotes and Shank3–/–:CaMKIIα-Cre hemizygotes were. (A) vGlut2+ inputs from dorsolateral geniculate nucleus of the thalamus (green) innervate both parvalbumin inhibitory neurons (red) and pyramidal neurons (blue) in layers II/III and IV. Shank3+/+:CaMKIIα-Cre hemizygotes and Shank3–/–:CaMKIIα-Cre hemizygotes'))

# resolve abbreviations in text
def resolve_abbreviation(content):
    other_replacements = {
            'V1': 'primary visual cortex',
            'V2': 'secondary visual cortex',
            'V3': 'tertiary visual cortex',
            'α':'alpha',
            'β':'betha',
            'γ':'gama',
            'δ':'delta',
            'Δ':'delta',
#            '1/2': '1-2',
#            '2/3': '2-3',
            }
    fulltext = '\n'.join(content) # putt all together
    abbr_pairs = extract_abbreviation_definition_pairs(doc_text=fulltext, most_common_definition=True)
    new_content = []
    for par in content:
        if len(par) == 0 or par == '\n': continue
        # replace found abbreviation
        for abbr, definition in abbr_pairs.items():
            if abbr.isupper():
                    par = par.replace(abbr, definition)
        # resolve roman numerals
        par = resolve_roman(par)
        # replace fix abbreviations
        for abbr, definition in other_replacements.items():
            par = par.replace(abbr, definition)
        # add to new content
        new_content.append(par)
    
    # return
    return new_content

# resolve abbreviations in text
def resolve_abbreviation2(article):
    other_replacements = {
            'V1': 'primary visual cortex',
            'V2': 'secondary visual cortex',
            'V3': 'tertiary visual cortex',
            'α':'alpha',
            'β':'betha',
            'γ':'gama',
            'δ':'delta',
            'Δ':'delta',
#            '1/2': '1-2',
#            '2/3': '2-3',
            }
    content = [par for lst in list(article.values()) for par in lst]
    fulltext = '\n'.join(content) # putt all together
    abbr_pairs = extract_abbreviation_definition_pairs(doc_text=fulltext, most_common_definition=True)
#    new_content = []
    new_article = {}
    for section, paragraphs in article.items():
        par_temp = []
        for par in paragraphs:
            if len(par) == 0 or par == '\n': continue
            # replace found abbreviation
            for abbr, definition in abbr_pairs.items():
                if abbr.isupper():
#                    par = par.replace(abbr, definition)
                    par = re.sub(r'\b{}\b'.format(re.escape(abbr)), definition, par)
            # resolve roman numerals
            par = resolve_roman(par)
            # replace fix abbreviations
            for abbr, definition in other_replacements.items():
                par = par.replace(abbr, definition)
            # add to new content
            par_temp.append(par)
        
        new_article[section] = par_temp
    
    # return
    return new_article
        

#cont = ["Shank3 -/-: CAMKII alpha-Cre Shank3+/+:CaMKIIα-Cre hemizygotes and Shank3–/–:CaMKIIα-Cre hemizygotes were obtained from Shank3+/–:CaMKIIα-Cre hemizygotes crossed with Shank3+/– mice. Ten-week-old male and female mice were handled by experimenters who were blinded to the genotypes and groups.", 'my principal investigator (PI) is Gio.', 'We are wroking on layer I and II/VI of hippocampus (Hp)', 'one of the main brain region of interest is V1']
#print(resolve_abbreviation(cont))

# =============================================================================
# %% polish and join terms
# =============================================================================
# join terms
def term_join(term_list):
    joined = ""
    f = False
    for idx, t in enumerate(term_list):
        if idx == 0:
            joined += t
        elif len(t) == 1 and not (t.isalpha() or t.isdigit()):
            joined += t
            f = True
        elif f:
            joined += t
            f = False
        else:
            joined += ' {}'.format(t)
    return joined

### to deal with plural forms
import inflect
pl = inflect.engine()
### our cases that need to stay fix
with open('./term-relation-extraction/neuro-fix-names.txt') as f:
    neuro_names = f.read()
neuro_fix = [n.lower() for n in neuro_names.splitlines()]
for ft in list(dict.fromkeys(neuro_fix)):
    pl.defnoun(ft, ft)
f.close()

# polish list from unwanted chars
def polish(term_list, category, ignore_list):
    gender =  [] # add genders
    if category == 'GEN':
        for term in term_list:
            term = term.lower()
            if term == 'm/f':
                gender.append('male/female')
#                term_list.remove('M/F')
            elif term in ['m', 'male', 'male-specific']:
                gender.append('male')
            elif term in ['f', 'female', 'mother', 'virgin']:
                gender.append('female')
            elif term in ['h', 'hermaphrodite']:
                gender.append('hermaphrodite')
            else: # conceptual match
                for g in ['sex', 'either', 'both', 'mix', 'same', 'unknown', 'male and female', 'female and male']:
                    if g in term:
                        gender.append('male/female')
                        break
                else:
                    gender.append(term)
        tmp = gender
    else:
        tmp = [str(t).lower() for t in term_list] # lower
    if category not in ['EXP', 'STR', 'AGE', 'STA', 'REG', 'CEL', 'REC', 'ORI']: # remove digits from terms
        tmp = [t.translate(remove_digits) for t in tmp]
    tmp = [t for t in tmp if len(t)] # remove empty strings
    if not len(tmp): # return if empty
        return tmp
    #tmp = [term_join(t.split()) for t in tmp] # remove unwanted spaces
    tmp_ = []
    for term in tmp:
        term_words = term.split()
        last = term_words.pop()
        if pl.singular_noun(last):
            last = pl.singular_noun(last)
        term_words.append(last)
        tmp_.append(term_join(term_words))
    tmp = tmp_ # singularized and remove spaced terms
    tmp = list(dict.fromkeys(tmp)) # keep uniq and preserve order
    tmp = [t for t in tmp if len(t) > 1 and t not in ignore_list]
    return tmp

# =============================================================================
# %% encode and decode strings to be key for MongoDB
# =============================================================================
def encodeKey(key):
    return key.replace("\\", "\\\\").replace("\$", "\\u0024").replace(".", "\\u002e")
def decodeKey(key):
    return key.replace("\\u002e", ".").replace("\\u0024", "\$").replace("\\\\", "\\")

# =============================================================================
# %% path correction
# =============================================================================
def path_normalizer(path):
    path = str(path).lower()
    path = path.replace(' ', '')
    path = path.replace('https', '')
    path = path.replace('http', '')
    path = path.replace('\\', '')
    path = re.sub("[|\/!@#$%^&*()*:{'}]", "", path)
    return path


# =============================================================================
# %% progress bar
# =============================================================================
def update_progress(progress, total, text = 'Progress'):  
    print('\r{0}: |{1:49}| {2:>2.2f}%'.format(text.title()[:10], '█' * int(50*progress/total), 100*(progress+1)/total), end='')
    if progress+1 == total:
        print() # new line at the end

# =============================================================================
# %% normalizer for mapping terms
# =============================================================================
def list_normalizer(input_list):
    # single term only
    if type(input_list) != list:
        single_term = True
        input_list = [input_list]
    tmp = list()
    for item in input_list:
        item = item.lower()
        item = re.sub(r'[^A-Za-z0-9 ]+', ' ', item)
        item = item.strip()
        if len(item) > 0:
            tmp.append(item)
    if single_term:
        return tmp[0] if len(tmp) else ''
    # normal list return
    return tmp

# =============================================================================
# %% closest term to terms in a list
# =============================================================================
def matchlib(term, term_list, term_maps={}, category='', sim=0.85):
    """
    Return closest term to a terms in a list, if not returns the original
    """
    term_originial = term
    translate = {'-':'-negative', '+':'-positive'}
    for ch in translate:
        if ch == term[-1]:
            term = term[:-1] + translate[ch]
            break
    similarity = [0.00] * len(term_list)
    match = False
    for idx, candid in enumerate(term_list):
        candid_split = candid.split()
        if term.lower() == "".join(e[0] for e in candid_split).lower(): # check if it matches the abbreivation
            similarity[idx] = 2 # abbreviation match
            match = True
#        ts = jaro_winkler(candid.lower(), term.lower())
        ts = jaro_winkler(candid.lower(), term.lower(), 0.03)
        if ts >= sim:
            match = True
            similarity[idx] = max(ts, similarity[idx])
    if match: # anything similar matched
        exact_sim_list = {}
        sim_list = {}
        for term, term_sim in zip(term_list, similarity):
            if term_sim >= 1:                
                exact_sim_list[term] = term_sim
            elif term_sim >= sim:
                sim_list[term] = term_sim
        if len(exact_sim_list):
            return exact_sim_list
        return sim_list #term_list[max_idx] # most similar
    elif len(term_maps): # check for mappings
            term = list_normalizer(term)
            sim_maps = {}
            if (term, category) in term_maps:
                for m in term_maps[(term, category)]['map']:
                    sim_maps[m] = 1.1
            if len(sim_maps):
                return sim_maps
    # no match and no map return original
    return {}#{term_originial:1} # no match is found


# test
#matchlib('praying kjkjjkj mantis', ['praying mantis (abcddd)', 'praying mantis (qs)', 'test', 'unrelevant'])
matchlib('controls', ['Control', 'praying mantis (qs)', 'test', 'unrelevant'])
#matchlib('a4 a6', [], local_term_maps, 'REG')

# =============================================================================
# %% remove elements from a list
# =============================================================================
def remove_values_from_list(the_list, remove_list):
    """
    Remvoe all occorances of remove_list from the_list
    """
    tmp = []
    remove_list = [r.lower() for r in remove_list]
    for val in the_list:
        if not isinstance(val, str): continue
        if val.lower() not in remove_list:
            tmp.append(val)
    return tmp

# =============================================================================
# %% NER search brute force
# =============================================================================
def bruteforce_ner(sentence, keys, term_db):
    """returns tag, start and end of the found entity."""  
    if not len(keys):
        found = {k:[] for k in list(set(term_db.values()))}
    else:
        found = {k:[] for k in keys}
    try:
        for term, cat in term_db.items():
            if len(term) == 0: continue
            # check for the term using regex
            if term[-1].isalnum():
                pattern = r'\b{}\b'.format(re.escape(term))
                match = re.search(pattern, sentence, flags=re.IGNORECASE)
                if match:
                    hit = match.group()
                    if hit not in found[cat]:
                        found[cat].append(hit)
            else:
                pattern = r'\b{}'.format(re.escape(term))
                match = re.search(pattern, sentence, flags=re.IGNORECASE)
                if match:
                    hit = match.group()
                    if hit not in found[cat]:
                        found[cat].append(hit)
        return found
    except Exception as e:
        error = '(BF-NER) Error while searching for patterns! {}'.format(str(e))
        print(error)
        return {}


# =============================================================================
# %% test area
# =============================================================================
def main():
    pass
#    s = "To understand how this M-L pattern is associated with the growth of laterally extending postsynaptic neuronal dendrites in the neuropil, we tracked dendritic morphology of caudal primary motor neurons during development."
#    bruteforce_ner(s, nmo_terms)
#    for i in range(50):
#        update_progress(i, 50, 'indicator')
#    print(remove_values_from_list([1000, '1','2','3','1','1','a','1'], ['1']))    
#    print(matchlib('mousee', ['mouse', 'cat', 'rat']))
#    cat = 'REC'
#    for t in final_ranked_entities[cat]:
#        print(t,'----' ,matchlib(t, polished_entities[cat]))
#   print(path_normalizer("https://do i.org/10.10  38/s41467-021// ////-22741-\\\\9\\\\"))
if __name__ == '__main__':
    main()
    
