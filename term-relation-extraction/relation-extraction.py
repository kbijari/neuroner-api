#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Jul  9 13:09:36 2021

@author: kayvan
"""

# =============================================================================
# %% imports
# =============================================================================
import urllib.parse
from pymongo import MongoClient
import pandas as pd
import ast
from collections import Counter
import os

### my imports
#import sys
#sys.path.append('../')
from auxiliary import remove_values_from_list, update_progress
from auxiliary import resolve_roman

# =============================================================================
# %% connect to db
# =============================================================================

#MONGO_HOST = "mongodb://root:metadata\@nmo2006@cng-nmo-dev6.orc.gmu.edu" #http://cng-nmo-dev6.orc.gmu.edu:27017/
MONGO_HOST = os.environ.get('DB_HOST', 'XXX-Specify-Your-Server') #"cng-nmo-dev6.orc.gmu.edu"
MONGO_PORT = 27017
MONGO_USER = os.environ.get('DB_USER', 'root') #"root"
MONGO_PASS = os.environ.get('DB_PASS', 'metadata@nmo2006') #"metadata@nmo2006"

print('connecting to ', MONGO_HOST)

connection = MongoClient(f'mongodb://{MONGO_USER}:{urllib.parse.quote_plus(MONGO_PASS)}@{MONGO_HOST}:{MONGO_PORT}') 
db = connection['nmo_terms'] # db name
db2 = connection['nmo_map'] # db name

# =============================================================================
# %% controls
# =============================================================================
cleanup = 0 # drop the table and clean the database
write = 1 # write to the database

# =============================================================================
# %% helper functions
# =============================================================================
def encodeKey(key):
    return key.replace("\\", "\\\\").replace("\$", "\\u0024").replace(".", "\\u002e")

def decodeKey(key):
    return key.replace("\\u002e", ".").replace("\\u0024", "\$").replace("\\\\", "\\")


def abbreviation_resolve(term, col):
    if col in ['gender', 'GEN']:
        if term == 'M':   return 'male'
        if term == 'F':   return 'female'
        if term == 'M/F': return 'male/female'
        if term == 'H':   return 'hermaphrodite'
    if col in ['age_scale']:
        if term.upper() == 'D':   return 'day'
        if term.upper() == 'M':   return 'month'
        if term.upper() == 'Y': return 'year'
    # not hit
    return term

# =============================================================================
# %% extraction
# =============================================================================
#grouped_metadata = pd.read_pickle('../ScaleUp/NMO8_UNQ.pkl')
#uniq_nmo_metadata = pd.read_csv('./NMO8.csv')
uniq_nmo_metadata = pd.read_csv('./term-relation-extraction/NMO20220801.csv')

ignore_list = ['nr','not', 'applicable', 'nan', 'reported', 'not reported', 'not applicable',]

column_list = ['species', 'strain_name', 'age_class',
       'age_scale', 'gender', 'region1', 'region2', 'region3', 'class1',
       'class2', 'class3', 'original_format', 'protocol', 'slicing_direction',
       'stain', 'objective_type', 'reconstruction_software', 'expercond']

column_map = {'species':'SPE', 'strain_name':'STR', 'age_class':'DEV',
       'age_scale':'AGE', 'gender':'GEN', 'region1':'REG', 'region2':'REG', 'region3':'REG', 'class1':'CEL',
       'class2':'CEL', 'class3':'CEL', 'original_format':'ORI', 'protocol':'PRO', 'slicing_direction':'SLI',
       'stain':'STA', 'objective_type':'OBJ', 'reconstruction_software':'REC', 'expercond':'EXP'}

col_counter = {}
for col in column_list:
    col_terms = remove_values_from_list(uniq_nmo_metadata[col].to_list(), ignore_list)
    col_counter[col] = Counter(col_terms)

all_terms = {}
for idx, row in uniq_nmo_metadata.iterrows():
    update_progress(idx, len(uniq_nmo_metadata), 'Extraction')
    for col in column_list:
#        for term in row[col]:
        term = row[col]
        # calculate term abundance
        term_abu = 1.0 * col_counter[col][term] / sum(col_counter[col].values())
        
        term_original = term
        if type(term) is not str: continue # skip non chars
        if term.lower() in ignore_list: continue # skip non chars
        # test
#        if col !='stain' and term.lower() == 'type ii': print(row)
        term = encodeKey(row[col])
        if term == 'type i': term = 'type I' # exception in NMO data
        term = abbreviation_resolve(term, col)
        term = resolve_roman(term) # change roman to int
        term = term.lower() # keep all terms low for standard
        pmid = row['PMID']
        archive = row['archive_name']
        bf_search = True
        pk = {} # primary key
        other = {} # to capture other relations
        # terms that need to be disregarded from the bf search
        if col in ['age_class', 'original_format']:
            bf_search = False
        for ch in [',', ';', '&', '@', '#']:
            if ch in term:
                bf_search = False
                break
        if term.lower() in ['large', 'small', 'right', 'left', 'custom', 'multiple']:
            bf_search = False
        
        # age_class
        if col == 'age_class':
            sp = encodeKey(row['species'])
            pk[sp] = 1
        # region
        # strain
        if col == 'strain_name':
            sp = row['species']
            sp = resolve_roman(sp).lower()
            sp = encodeKey(sp)
            pk[sp] = 1
        # region
        if col == 'region3':
            r1 = resolve_roman(row['region1']).lower()
            r2 = resolve_roman(row['region2']).lower()
            p = encodeKey("{}-->>{}".format(r1, r2))
            pk[p] = 1
        if col == 'region2':
            r1 = encodeKey(row['region1'])
            r1 = resolve_roman(r1).lower()
            r1 = encodeKey(r1)
            pk[r1] = 1
        # celltype
        if col == 'class3':
            c1 = resolve_roman(row['class1']).lower()
            c2 = resolve_roman(row['class2']).lower()
            p = encodeKey("{}-->>{}".format(c1, c2))
            pk[p] = 1
        if col == 'class2':
            c1 = resolve_roman(row['class1']).lower()
            c1 = encodeKey(c1)
            pk[c1] = 1
        # other relations
        if col in ['species']:
            k = encodeKey(row['region1'])
            if k.lower() not in ignore_list:
                other[k] = {'count': 1, 'category':'REG'}
            k = encodeKey(row['class1'])
            if k.lower() not in ignore_list:
                other[k] = {'count': 1, 'category':'CEL'}
            k = encodeKey(row['protocol'])
            if k.lower() not in ignore_list:
                other[k] = {'count': 1, 'category':'PRO'}
        if col in ['class1', 'region1']:
            k = encodeKey(row['species'])
            if k.lower() not in ignore_list:
                other[k] = {'count': 1, 'category':'SPE'}

            
        if term not in all_terms.keys():
            tmp = {'term':term,
                   'term_original':term_original,
#                   'term_lowercase':term.lower(),
                   'count':1,
                   'term_abu': term_abu,
                   'pk': pk,
                   'bf-search':bf_search,
                   'category': column_map[col],
                   'nmo_category': [col],
                   'other-relations': other,
                   'pmid': [pmid],
                   'archive': [archive],
                   'positive-rule': {},
                   'negative-rule': {},
                    }
            all_terms[term] = tmp
        else:
            if archive not in all_terms[term]['archive']:
                all_terms[term]['archive'].append(archive)
            if pmid not in all_terms[term]['pmid']:
                all_terms[term]['pmid'].append(pmid)
                all_terms[term]['count'] = len(all_terms[term]['pmid'])
            if col not in all_terms[term]['nmo_category']:
                all_terms[term]['nmo_category'].append(col)
            # depend on
            for p in pk.keys():
                if p not in all_terms[term]['pk'].keys():
                    all_terms[term]['pk'][p] = pk[p]
                else:
                    all_terms[term]['pk'][p] += pk[p]
            # other relations
            for k in other.keys():
                if str(k).lower() in ignore_list: continue
                if k not in all_terms[term]['other-relations'].keys():
                    all_terms[term]['other-relations'][k] = other[k]
                else:
                    all_terms[term]['other-relations'][k]['count'] += other[k]['count']

# =============================================================================
# %% rules
# =============================================================================
# read the rules from file
rules = pd.read_csv('./term-relation-extraction/rules.csv')

for idx, rule in rules.iterrows():
    update_progress(idx, len(rules), 'NegationRule')
#    print(rule)
    ant, ant_cat = rule['antecedent'].split('_(')
    con, con_cat = rule['consequent'].split('_(')
    ant_cat, con_cat = ant_cat[:-1], con_cat[:-1]
    # standardize
    ant, con = abbreviation_resolve(ant, ant_cat), abbreviation_resolve(con, con_cat)
    rule_type = rule['rule_type']
    rule_category = rule['category']
    rule_reasoning = rule['reasoning']
    
    # encode keys
    ant, con = encodeKey(ant), encodeKey(con)
    # add the rule
    if ant in all_terms.keys():
        # clean 
        all_terms[ant]['positive-rule'] = {}
        all_terms[ant]['negative-rule'] = {}
        # assign rules
        if rule_type == 'negative':
            tmp = {'category':con_cat, 'reasoning':rule_reasoning, 'rule_category':rule_category}
            if con not in all_terms[ant]['negative-rule'].keys():
                all_terms[ant]['negative-rule'][con] = tmp
        if rule_type == 'positive':
            tmp = {'category':con_cat, 'reasoning':rule_reasoning, 'rule_category':rule_category}
            if con not in all_terms[ant]['positive-rule'].keys():
                all_terms[ant]['positive-rule'][con] = tmp
    # add the reverse of the rule
    if con in all_terms.keys():
        # assign rules
        if rule_type == 'negative':
            tmp = {'category':ant_cat, 'reasoning':rule_reasoning, 'rule_category':rule_category}
            if ant not in all_terms[con]['negative-rule'].keys():
                all_terms[con]['negative-rule'][ant] = tmp
        if rule_type == 'positive':
            tmp = {'category':ant_cat, 'reasoning':rule_reasoning, 'rule_category':rule_category}
            if ant not in all_terms[con]['positive-rule'].keys():
                all_terms[con]['positive-rule'][ant] = tmp
        
    

# =============================================================================
# %% write to the cloud
# =============================================================================
### write to the database
#write = 0
if cleanup:
    db.term_db.drop() # drop the collection
if write:
    print('writing into the database...')
    db.term_db.insert_many(all_terms.values()) # insert all terms in the collection


# =============================================================================
# %% extract and write mapping data
# =============================================================================

sheet_to_df_map = pd.read_excel('./term-relation-extraction/combination_stats_KB092921.xlsx', sheet_name=None)
map_data = pd.concat(sheet_to_df_map, axis=0, ignore_index=True)

map_data = map_data[map_data['map'] == 1]

term_maps = {}
for t in list(db.term_maps.find({}, {"_id":0})):
    term_maps[t['term']] = t

for idx, row in map_data.iterrows():
    term = row['term-co']
    if term not in term_maps:
        term_maps[term] = {'term': term, 'category': row['category'], 'map':[row['term']]}
    else:
        if row['term'] not in term_maps[term]['map']:
            term_maps[term]['map'].append(row['term'])


if cleanup:
    db.term_maps.drop() # clean the db
if write:
    db.term_maps.insert_many(term_maps.values()) # write to db
    
# =============================================================================
# %% extract and write mapping data
# =============================================================================

sheet_to_df_map = pd.read_excel('./term-relation-extraction/miss_candids_KB101921.xlsx', sheet_name=None)
map_data = pd.concat(sheet_to_df_map, axis=0, ignore_index=True)

# filter to those there is a map for
map_data = map_data[map_data['chosen'].str.len() > 2]

term_maps = {}
for t in list(db.term_maps.find({}, {"_id":0})):
    term_maps[t['term']] = t

for idx, row in map_data.iterrows():
#    term = row['term']
    chosen_terms = [t.strip() for t in ast.literal_eval(row['chosen'])]
    for term in chosen_terms:
        if term not in term_maps:
            term_maps[term] = {'term': term, 'category': row['category'], 'map':[row['term']]}
        else:
            if row['term'] not in term_maps[term]['map']:
                term_maps[term]['map'].append(row['term'])


if cleanup:
    db.term_maps.drop() # clean the db
if write:
    db.term_maps.insert_many(term_maps.values()) # write to db
    
    


