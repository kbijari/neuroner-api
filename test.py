import os
import sys
from pymongo import MongoClient
import urllib
import parameters

try:
    username = urllib.parse.quote_plus('root')
    password = urllib.parse.quote_plus('metadata@nmo2006')
    db_host = 'localhost'
    mongo_client = MongoClient('mongodb://%s:%s@%s:27017' % (username, password, db_host))
    print(username, password, db_host)
    print(mongo_client.server_info())
except Exception as e:
    print('Error calling Mongo!', str(e))
    sys.exit(-1)
