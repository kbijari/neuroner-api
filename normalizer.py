#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Mar 30 12:57:41 2020

@author: kayvan
"""
# functions for text normalization

# =============================================================================
# %% imports
# =============================================================================
import re, unicodedata
#import nltk
#import contractions
# import inflect
#from bs4 import BeautifulSoup
from nltk import word_tokenize, sent_tokenize
#from nltk.corpus import stopwords
#from nltk.stem import LancasterStemmer, WordNetLemmatizer

# import spacy
# spacy_nlp = spacy.load('en_core_web_sm')

# =============================================================================
# %% functions
# =============================================================================

def remove_non_ascii(words):
    """Remove non-ASCII characters from list of tokenized words"""
    new_words = []
    for word in words:
        try:
            new_word = unicodedata.normalize('NFKD', word).encode('ascii', 'ignore').decode('utf-8', 'ignore')
            new_words.append(new_word)
        except Exception as e:
            pass
#            print(word)
    return new_words

def to_lowercase(words):
    """Convert all characters to lowercase from list of tokenized words"""
    new_words = []
    for word in words:
        new_word = word.lower()
        new_words.append(new_word)
    return new_words

def remove_punctuation(words):
    """Remove punctuation from list of tokenized words"""
    new_words = []
    for word in words:
        new_word = re.sub(r'[^\w\s]', '', word)
        if new_word != '':
            new_words.append(new_word)
    return new_words

def spaced_punctuation(words):
    """ add a single space befor punctuations"""
    new_words = []
    for word in words:
#        new_word = re.sub('(?<! )(?=/*-+[.,!?()])|(?<=[.,!?()])(?! )', r' ', word)
        new_word = re.sub('([.,!?()])', r'\1', word)
        new_word = re.sub('\s{2,}', ' ', new_word)
        new_words.append(new_word)
    return new_words

# def replace_numbers(words):
#     """Replace all interger occurrences in list of tokenized words with textual representation"""
#     p = inflect.engine()
#     new_words = []
#     for word in words:
#         if word.isdigit():
#             new_word = p.number_to_words(word)
#             new_words.append(new_word)
#         else:
#             new_words.append(word)
#     return new_words

#def remove_stopwords(words):
#    """Remove stop words from list of tokenized words"""
#    new_words = []
#    for word in words:
#        if word not in stopwords.words('english'):
#            new_words.append(word)
#    return new_words

#def stem_words(words):
#    """Stem words in list of tokenized words"""
#    stemmer = LancasterStemmer()
#    stems = []
#    for word in words:
#        stem = stemmer.stem(word)
#        stems.append(stem)
#    return stems
#
#def lemmatize_verbs(words):
#    """Lemmatize verbs in list of tokenized words"""
#    lemmatizer = WordNetLemmatizer()
#    lemmas = []
#    for word in words:
#        lemma = lemmatizer.lemmatize(word, pos='v')
#        lemmas.append(lemma)
#    return lemmas

def word_list_normalize(words):
    """Normalize a list of words"""
    words = remove_non_ascii(words)
    # words = to_lowercase(words)
#    words = remove_punctuation(words)
    # words = spaced_punctuation(words)
    # words = replace_numbers(words)
#    words = stem_words(words)
#    words = lemmatize_verbs(words)
#    words = remove_stopwords(words)
    return words

def sentence_normalize(sent):
    """Normalize a sentence"""
    words = word_tokenize(sent)
    # words = [token.text for token in spacy_nlp(sent)]
    new_words = word_list_normalize(words)
    beforafter = ""
    new_sent = beforafter + " ".join(new_words) + beforafter # add initial and ending space
    return new_sent

# test
def main():
    words = word_list_normalize(['This', 'IS,'])
    print(words)
    
    sent = '''this is KaYvan's [Sentence], which+ contains punctuation! & (abbrs.) and some monkeys + 29 toghether with some c57bl/6j rats-rabbits.'''
    print(sentence_normalize(sent))
   
if __name__ == '__main__':
    main()
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
 